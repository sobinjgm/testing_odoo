# -*- encoding: utf-8 -*-
##############################################################################
#
# Released under LGPL v.3
#
##############################################################################

{
    "name": "HR Qatar",
    "version": "1.1",
    "category": "HR",
    "description": """
			Emoployee management for Qatar
		""",
    "author": "Infintor",
    "website": "",
    "depends": [
        "base","hr"
    ],
    'data': [
     #   'security/ir.model.access.csv',
        'views/views.xml',
    ],
    "init_xml": [],
    "update_xml": [],
    "demo_xml": [],
    "test": [],
    "installable": True,
    "active": False,
	'license' : 'LGPL-3',
}
