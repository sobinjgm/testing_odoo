# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class Employee(models.Model):
    _inherit = 'hr.employee'

    emp_sponsor = fields.Many2one('res.partner', string="Sponsor")
    emp_id = fields.Char(string="Employee ID", required=True)
    passport_expiry = fields.Date(string="Passport Expiry", required=True)
    passport_id = fields.Char(required=True)
    visa_no = fields.Char(string="Visa No", required=True)
    visa_expiry = fields.Date(string="Visa Expiry", required=True)
    qatar_id = fields.Char(string="Qatar ID")
    qatar_id_expiry = fields.Date(string="Qatar ID Expiry")
    date_join = fields.Date(string="Date of Joining")
    date_landing = fields.Date(string="Date of Landing")
    emergency_contact_no = fields.Char(string="Emergency Contact", required=True)
    emergency_contact = fields.Char(string="Emergency Contact Person", required=True)
    driving_license = fields.Boolean(string="Driving License?")
    license_expiry = fields.Date(string="License Expiry")
    state = fields.Selection([
        ('temporary', 'Temporary'),
        ('medical', 'Medical'),
        ('fingerprint', 'Fingerprint'),
        ('visa', 'RP'),
        ('probation', 'Probation'),
        ('permanent', 'Permanent'),
        ('absconded', 'Absconded'),
        ('terminated', 'Terminated'),
        ('resigned', 'Resigned'),
        ('deported', 'Deported'),
        ('departed', 'Departed'),
        ('settled', 'Settled'),
        ('sponsor changed', 'Sponsor Changed')
    ],
        'Status', readonly=True, default='temporary', copy=False, help="Gives the statues of employee", select=True)

    emp_status = fields.Selection([('none', 'None'),
                              ('probation', 'Probation'),
                              ('permanent', 'Permanent'),
                              ('temporary', 'Temporary'),
                              ('terminated', 'Terminated'),
                              ('absconded', 'Absconded'),
                              ('resigned', 'Resigned'),
                              ('deported', 'Deported'),
                              ('settled', 'Settled'),
                              ('departed', 'Departed'),
                              ('sponsor changed', 'Sponsor Changed')], readonly=True, default='none',
                             string='Employment Status')
    emp_date = fields.Date(string="Status Date", readonly=True)
    emp_note = fields.Text('Comments')
    emp_attachment_ids = fields.Many2many('ir.attachment', 'emp_attachment_rel', 'emp_id',
                                     'attachment_id', 'Related Documents',
                                     help="You may attach documents related to employment status")

    med_status = fields.Selection([('none', 'None'),
                              ('appear', 'To appear'),
                              ('passed', 'Passed'),
                              ('failed', 'Failed'),
                              ('reappear', 'Reappear'),
                              ], string='Medical Status', default='none', readonly=True)
    med_duration = fields.Integer(string="Duration to complete")
    med_date = fields.Date(string="Status Date", readonly=True)
    med_note = fields.Text('Comments')
    med_attachment_ids = fields.Many2many('ir.attachment', 'med_attachment_rel', 'med_id',
                                     'attachment_id', 'Related Documents',
                                     help="You may attach documents related to medical status")

    visa_status = fields.Selection([('none', 'None'),
                               ('used in country', 'Used in country'),
                               ('expired', 'Expired'),
                               ('issued ID', 'Issued ID'),
                               ], string='Visa Status', default='none', readonly=True)
    visa_date = fields.Date(string="Status Date", readonly=True)
    visa_note = fields.Text('Comments')
    visa_attachment_ids = fields.Many2many('ir.attachment', 'visa_attachment_rel', 'visa_id',
                                      'attachment_id', 'Related Documents',
                                      help="You may attach documents related to visa status")
    finger_status = fields.Selection([('none', 'None'),
                                 ('passed', 'Passed'),
                                 ('failed', 'Failed'),
                                 ('reappear', 'Reappear'),
                                 ], string='Fingerprint Status', default='none', readonly=True)
    finger_date = fields.Date(string="Status Date", readonly=True)
    finger_note = fields.Text('Comments')
    finger_attachment_ids = fields.Many2many('ir.attachment', 'finger_attachment_rel', 'finger_id',
                                        'attachment_id', 'Related Documents',
                                        help="You may attach documents related to finger print status")
    bank_account_no = fields.Char('Account Number')
    bank_id = fields.Many2one('res.bank',string='Bank')
    bank_transfer_type = fields.Selection([('pay_card', 'Pay Card Transfer'),
                                 ('own_bank', 'Own Bank Transfer'),
                                 ('other_bank', 'Other Bank Transfer'),
                                 ], string='Transfer Type')



    @api.multi
    def write(self, vals):

        for emp in self:
            changes = []
            if 'state' in vals and emp.state != vals['state']:
                old_state = emp.state or _('None')
                changes.append(_("Status: from '%s' to '%s'") % (old_state, vals['state']))
            if 'emp_date' in vals and emp.emp_date != vals['emp_date']:
                old_emp_date = emp.emp_date or _('None')
                changes.append(_("Employment Status date: from '%s' to '%s'") % (old_emp_date, vals['emp_date']))
            if 'emp_status' in vals and emp.emp_status != vals['emp_status']:
                old_emp_status = emp.emp_status or _('None')
                changes.append(_("Employment Status: from '%s' to '%s'") % (old_emp_status, vals['emp_status']))

            if 'med_date' in vals and emp.med_date != vals['med_date']:
                old_med_date = emp.med_date or _('None')
                changes.append(_("Medical Status date: from '%s' to '%s'") % (old_med_date, vals['med_date']))
            if 'med_status' in vals and emp.med_status != vals['med_status']:
                old_med_status = emp.med_status or _('None')
                changes.append(_("Medical Status: from '%s' to '%s'") % (old_med_status, vals['med_status']))
            if 'visa_date' in vals and emp.visa_date != vals['visa_date']:
                old_visa_date = emp.visa_date or _('None')
                changes.append(_("Visa Status date: from '%s' to '%s'") % (old_visa_date, vals['visa_date']))
            if 'visa_status' in vals and emp.visa_status != vals['visa_status']:
                old_visa_status = emp.visa_status or _('None')
                changes.append(_("Visa Status: from '%s' to '%s'") % (old_visa_status, vals['visa_status']))
            if 'finger_date' in vals and emp.finger_date != vals['finger_date']:
                old_finger_date = emp.finger_date or _('None')
                changes.append(_("Fingerprint Status date: from '%s' to '%s'") % (old_finger_date, vals['finger_date']))
            if 'finger_status' in vals and emp.finger_status != vals['finger_status']:
                old_finger_status = emp.finger_status or _('None')
                changes.append(_("Fingerprint Status: from '%s' to '%s'") % (old_finger_status, vals['finger_status']))

            if len(changes) > 0:
                emp.message_post(body=", ".join(changes))

        emp_id = super(Employee, self).write(vals)
        return True


class emp_enter_id(models.TransientModel):
    _name = "emp.enter.id"
    _description = "Enter Employee ID"

    date_issued = fields.Date(string="Issued on", required=True)
    emp_id = fields.Char(string="Qatar ID", required=True)
    date_expiry = fields.Date(string="Qatar ID Expiry", required=True)


    @api.multi
    def enter_id(self):

        emp_obj = self.env['hr.employee']

        for data in self:
            if not data.emp_id:
                raise UserError(_('Enter a valid ID.'))
            ctx = self.env.context.copy()
            e_id = ctx.get('active_id')
            if e_id:
                emp_obj.browse([e_id]).write(
                              {'qatar_id': data.emp_id, 'qatar_id_expiry': data.date_expiry, 'visa_status': 'issued ID',
                               'visa_date': data.date_issued})
        return {}


class emp_exit(models.TransientModel):
    _name = "emp.exit"
    _description = "Employee Exit"


    emp_state = fields.Selection([('probation', 'Probation'),
                                       ('absconded', 'Absconded'),
                                       ('permanent', 'Permanent'),
                                       ('temporary', 'Temporary'),
                                       ('terminated', 'Terminated'),
                                       ('resigned', 'Resigned'),
                                       ('deported', 'Deported'),
                                       ('settled', 'Settled'),
                                       ('departed', 'Departed'),
                                       ('sponsor changed', 'Sponsor Changed')], string='Employment Status')


    @api.multi
    def e_exit(self):

        emp_obj = self.env['hr.employee']

        for data in self:
            if not data.emp_state:
                raise UserError(_('Enter a valid state'))
            ctx = self.env.context.copy()
            e_id = ctx.get('active_id')
            if e_id:
                emp_obj.browse([e_id]).write({'state': data.emp_state})
        return {}


class emp_status(models.TransientModel):
    _name = "emp.status"
    _description = "Employment Status"

    emp_date = fields.Date(string="Status date", required=True)
    emp_state = fields.Selection([('probation', 'Probation'),
                                       ('absconded', 'Absconded'),
                                       ('permanent', 'Permanent'),
                                       ('temporary', 'Temporary'),
                                       ('terminated', 'Terminated'),
                                       ('resigned', 'Resigned'),
                                       ('deported', 'Deported'),
                                       ('settled', 'Settled'),
                                       ('departed', 'Departed'),
                                       ('sponsor changed', 'Sponsor Changed')], required=True,
                                      string='Employment Status')


    @api.multi
    def e_status(self):


        emp_obj = self.env['hr.employee']

        for data in self:
            if not data.emp_state:
                raise UserError(_('Enter a valid state'))
            ctx = self.env.context.copy()
            e_id = ctx.get('active_id')
            if e_id and data.emp_state in ['terminated', 'resigned']:
                emp_obj.browse([e_id]).write(
                              {'state': data.emp_state, 'emp_date': data.emp_date, 'emp_status': data.emp_state,
                               'active': False})
            elif e_id:
                emp_obj.browse([e_id]).write(
                              {'state': data.emp_state, 'emp_date': data.emp_date, 'emp_status': data.emp_state},
                              )

        return {}


class emp_medical(models.TransientModel):
    _name = "emp.medical"
    _description = "Enter Employee medical date"

    date_med = fields.Date(string="Appearing on", required=True)

    @api.multi
    def enter_med_date(self):

        emp_obj = self.env['hr.employee']

        for data in self:
            if not data.date_med:
                raise UserError(_('Enter a valid date'))
            ctx = self.env.context.copy()
            e_id = ctx.get('active_id')
            if e_id:
                emp_obj.browse([e_id]).write({'med_date': data.date_med, 'med_status': 'appear', 'state': 'medical'},
                              )
        return {}


class emp_medical_passed(models.TransientModel):
    _name = "emp.medical.passed"
    _description = "Enter employee medical passed date"

    date_med_passed = fields.Date(string="Passed on", required=True)

    @api.multi
    def enter_med_passed_date(self):


        emp_obj = self.env['hr.employee']

        for data in self:
            if not data.date_med_passed:
                raise UserError(_('Enter a valid date'))
            ctx = self.env.context.copy()
            e_id = ctx.get('active_id')
            if e_id:
                emp_obj.browse([e_id]).write(
                              {'med_date': data.date_med_passed, 'med_status': 'passed', 'state': 'fingerprint'},
                              )
        return {}


class emp_medical_failed(models.TransientModel):
    _name = "emp.medical.failed"
    _description = "Enter employee medical failed date"

    date_med_failed = fields.Date(string="Failed on", required=True)

    @api.multi
    def enter_med_failed_date(self):


        emp_obj = self.env['hr.employee']

        for data in self:
            if not data.date_med_failed:
                raise UserError(_('Enter a valid date'))
            ctx = self.env.context.copy()
            e_id = ctx.get('active_id')
            if e_id:
                emp_obj.browse([e_id]).write(
                              {'med_date': data.date_med_failed, 'med_status': 'failed', 'state': 'medical'},
                              )
        return {}


class emp_medical_reappear(models.TransientModel):
    _name = "emp.medical.reappear"
    _description = "Enter employee medical reappear date"

    date_med_reappear = fields.Date(string="Re-appear on", required=True)

    @api.multi
    def enter_med_reappear_date(self):

        emp_obj = self.env['hr.employee']

        for data in self:
            if not data.date_med_failed:
                raise UserError(_('Enter a valid date'))
            ctx = self.env.context.copy()
            e_id = ctx.get('active_id')
            if e_id:
                emp_obj.browse([e_id]).write(
                              {'med_date': data.date_med_reappear, 'med_status': 'reappear', 'state': 'medical'},
                              )
        return {}


class emp_finger_passed(models.TransientModel):
    _name = "emp.finger.passed"
    _description = "Enter employee finger passed date"

    date_finger_passed = fields.Date(string="Passed on", required=True)

    @api.multi
    def enter_finger_passed_date(self):

        emp_obj = self.env['hr.employee']

        for data in self:
            if not data.date_finger_passed:
                raise UserError(_('Enter a valid date'))
            ctx = self.env.context.copy()
            e_id = ctx.get('active_id')
            if e_id:
                emp_obj.browse([e_id]).write(
                              {'finger_date': data.date_finger_passed, 'finger_status': 'passed', 'state': 'visa'},
                             )
        return {}


class emp_finger_failed(models.TransientModel):
    _name = "emp.finger.failed"
    _description = "Enter employee fingerprint failed date"

    date_finger_failed = fields.Date(string="Failed on", required=True)

    @api.multi
    def enter_finger_failed_date(self):


        emp_obj = self.env['hr.employee']

        for data in self:
            if not data.date_finger_failed:
                raise UserError(_('Enter a valid date'))
            ctx = self.env.context.copy()
            e_id = ctx.get('active_id')
            if e_id:
                emp_obj.browse([e_id]).write({'finger_date': data.date_finger_failed, 'finger_status': 'failed',
                                              'state': 'fingerprint'})
        return {}


class emp_finger_reappear(models.TransientModel):
    _name = "emp.finger.reappear"
    _description = "Enter employee medical reappear date"

    date_finger_reappear = fields.Date(string="Re-appear on", required=True)

    @api.multi
    def enter_finger_reappear_date(self):


        emp_obj = self.env['hr.employee']

        for data in self:
            if not data.date_finger_reappear:
                raise UserError(_('Enter a valid date'))
            ctx = self.env.context.copy()
            e_id = ctx.get('active_id')
            if e_id:
                emp_obj.browse([e_id]).write({'finger_date': data.date_finger_reappear, 'finger_status': 'reappear',
                                              'state': 'fingerprint'})
        return {}


class emp_id_expired(models.TransientModel):
    _name = "emp.id.expired"
    _description = "Enter Qatar ID expired date"

    date_id_expired = fields.Date(string="Expired on", required=True)

    @api.multi
    def enter_id_expired_date(self):


        emp_obj = self.env['hr.employee']

        for data in self:
            if not data.date_id_expired:
                raise UserError(_('Enter a valid date'))
            ctx = self.env.context.copy()
            e_id = ctx.get('active_id')
            if e_id:
                emp_obj.browse([e_id]).write(
                              {'visa_date': data.date_id_expired, 'visa_status': 'expired', 'state': 'visa'},
                              )
        return {}
