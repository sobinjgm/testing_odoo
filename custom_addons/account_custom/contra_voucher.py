from odoo.tools.translate import _
from odoo.models import fields, models


class account_journal(osv.osv):
    _inherit = "account.journal"
   
    default_contra_voucher_account = fields.many2one('account.account', 'Contra Voucher Account'),
                

account_journal()


class contra_voucher(models.Model):
    _name = 'contra.voucher'
        'name': fields.Char('Reference', size=64, required=False,readonly=True, states={'draft': [('readonly', False)]}),
        'company_id': fields.Many2one('res.company', 'Company', required=True,readonly=True, states={'draft': [('readonly', False)]}),
        'document_no': fields.Char('Document No.', size=124, readonly=True, states={'draft': [('readonly', False)]}),
        'document_date': fields.Date('Document Date', readonly=True, states={'draft': [('readonly', False)]}),
        'bank_name': fields.Char('Bank', size=124, readonly=True, states={'draft': [('readonly', False)]}),
        'document_deposit_date': fields.Date('Document Deposit Date', readonly=True,
                                             states={'draft': [('readonly', False)]}),
        'document_clearing_date': fields.Date('Document Clearing Date', readonly=True,
                                              states={'draft': [('readonly', False)]}),
        'journal_id1': fields.Many2one('account.journal', 'Source Journal', required=True,readonly=True, states={'draft': [('readonly', False)]}),
        'journal_id2': fields.Many2one('account.journal', 'Destination Journal', required=True,readonly=True, states={'draft': [('readonly', False)]}), 
        'date_journal': fields.Date('Date', required=True,readonly=True, states={'draft': [('readonly', False)]}),
        'amount': fields.Float('Amount', required=True,readonly=True, states={'draft': [('readonly', False)]}),
        'move_id1': fields.Many2one('account.move', 'Credit Entry', required=False,readonly=True, states={'draft': [('readonly', False)]}),
        'move_id2': fields.Many2one('account.move', 'Debit Entry', required=False,readonly=True, states={'draft': [('readonly', False)]}),

        'state': fields.Selection([
            ('draft', 'Draft'),  
            ('confirm', 'Confirmed'),
            ('cancel', 'Cancelled'),
            ], 'Status', readonly=True,select=True),
        'comment':fields.text('Comment', readonly=True, states={'draft': [('readonly', False)]}),
              
        
#    _defaults = {       
#                'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
#                'state':'draft',
#                'date_journal': fields.date.context_today,
#    }.............................????

        
    def create_credit_entry(self,ids, context=None):
        period_obj = self.env['account.period']
        move_obj = self.env['account.move']
        move_line_obj = self.env('account.move.line')
        ctx = {}
        for record in self.browse(ids, context=context):

            if not record.journal_id1.default_contra_voucher_account:
#                raise osv.except_osv(_('No Contra-voucher Account!'), _('Please configure Contra-Voucher Account at Journal Configuration - '+record.journal_id1.name))................???
            period_id =  False
            ctx.update(company_id=record.company_id.id,account_period_prefer_normal=True)
            if not period_id:
               period_ids = period_obj.find(record.date_journal, context=ctx)
               period_id = period_ids and period_ids[0] or False

            move = {
            'ref': record.name and record.name or record.name,
            'journal_id': record.journal_id1.id,
            'company_id': record.company_id.id,
            'date': record.date_journal,
            'narration':record.comment,
            'period_id':period_id,
            }

            journal_entry_id = move_obj.create(move, context=ctx)

            vals1=  {
             'partner_id': record.company_id.partner_id.id,
             'name': record.name or '',
             'date': record.date_journal,
             'debit': record.amount>0 and record.amount,
             'credit': 0.0,
             'account_id': record.journal_id1.default_contra_voucher_account and record.journal_id1.default_contra_voucher_account.id or False,
             'period_id':period_id,
             'move_id': journal_entry_id,
             'company_id': record.company_id.id,
             }
            move_line_obj.create(vals1,context)
            vals2=  {
             'partner_id': record.company_id.partner_id.id,
             'name': record.name or '',
             'date': record.date_journal,
             'debit': 0.0,
             'credit': record.amount>0 and record.amount,
             'account_id': record.journal_id1.default_credit_account_id and record.journal_id1.default_credit_account_id.id or False,
             'period_id':period_id,
             'move_id': journal_entry_id,
              'company_id': record.company_id.id,
             }
            move_line_obj.create(vals2,context)

            if record.journal_id1.entry_posted:
                move_obj.button_validate([journal_entry_id], context=None)
        res = self.write(ids, {'move_id1': journal_entry_id})
        return res 
    
    def create_debit_entry(self,ids, context=None):
        period_obj = self.env['account.period']
        move_obj = self.env['account.move']
        move_line_obj = self.env('account.move.line')
        ctx = {}
        for record in self.browse(ids):

            period_id =  False
            ctx.update(company_id=record.company_id.id,account_period_prefer_normal=True)
            if not period_id:
               period_ids = period_obj.find( record.date_journal, context=ctx)#.............?
               period_id = period_ids and period_ids[0] or False

            move = {
                'ref': record.name,
                'journal_id': record.journal_id2.id,
                'company_id': record.company_id.id,
                'date': record.date_journal,
                'narration': record.comment,
                'period_id': period_id,
            }

            journal_entry_id = move_obj.create(move, context=ctx)
            vals1=  {
             'date_maturity': False,
             'partner_id': record.company_id.partner_id.id,
             'name': record.name  or '',
             'date': record.date_journal,
             'debit': record.amount>0 and record.amount,
             'credit': 0.0,
             'account_id': record.journal_id2.default_debit_account_id and record.journal_id2.default_debit_account_id.id or False,
             'company_id': record.company_id.id,
             'move_id':journal_entry_id
             }                
            move_line_obj.create(vals1,context)#...............?
            vals2=  {
             'date_maturity': False,
             'partner_id': record.company_id.partner_id.id,
             'name': record.name or '',
             'date': record.date_journal,
             'debit': 0.0,
             'credit': record.amount>0 and record.amount,
             'account_id': record.journal_id1.default_contra_voucher_account and record.journal_id1.default_contra_voucher_account.id or False,
             'period_id':period_id,
             'company_id': record.company_id.id,
             'move_id':journal_entry_id
             }                
            move_line_obj.create(vals2,_context)#........................?

            if record.journal_id2.entry_posted:
                move_obj.button_validate([journal_entry_id], context=None)
        res = self.write(ids, {'state': 'confirm','move_id2': journal_entry_id})
        return res


    def button_confirm(self,ids, _context=None):
        for record in self.browse(ids):
            record.create_credit_entry()
            record.create_debit_entry()
        return True

    def button_cancel(self,ids, _context=None):       
        move_obj = self.env['account.move']
        for record in self.browse(ids):
            if record.move_id1:
                move_obj.unlink([record.move_id1.id])
            if record.move_id2:
                move_obj.unlink( [record.move_id2.id])
            res = self.write(ids, {'state': 'cancel'}) 
        return True

    def unlink(self,ids, _context=None):   
        entries = self.read(ids, ['state'])
        unlink_contra_ids = []
        for record in entries:
            if record['state'] in ['draft']:
                unlink_contra_ids.append(record['id'])
            else:
#                raise osv.except_osv(_('Invalid Action!'), _('You cannot delete entries which is not in draft state'))................................????
        return super(contra_voucher, self).unlink( unlink_contra_ids)

contra_voucher()
