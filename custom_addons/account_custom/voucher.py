# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################



from odoo.models import  models

#

class account_voucher(Models.model):
    _inherit = "account.voucher"#..........................................?
    
     cheque_no = fields.Char('Cheque No.', size=124),
     cheque_date = fields.Date('Cheque Date'),
     bank_branch = fields.Char('Bank & Branch', size=124),
     cheque_deposit_date = fields.Date('Cheque Deposit Date'),
     cheque_clearing_date = fields.Date('Cheque Clearing Date'),
     cheque_remarks = fields.Text('Remarks'),

            


    def confirm_voucher(self,ids, context=None):

        for record in self.browse(ids):
            total_amount =0
            if record.line_ids:

                for line in record.line_ids:
                    total_amount += line.amount
            record.write({'amount':total_amount})
            print 'hai'

            self.signal_workflow(ids, 'proforma_voucher')

        return True

account_voucher()
