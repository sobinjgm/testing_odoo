import itertools
import math
from lxml import etree

from odoo import models, fields, api, _
#from openerp.exceptions import except_orm, Warning, RedirectWarning
#from odoo.exceptions import UserError
from odoo.tools import float_compare
import odoo.addons.decimal_precision as dp

# mapping invoice type to journal type
TYPE2JOURNAL = {
    'out_invoice': 'sale',
    'in_invoice': 'purchase',
    'out_refund': 'sale_refund',
    'in_refund': 'purchase_refund',
}

class account_invoice(models.Model):
    _name = "account.invoice"


    date_invoice = fields.Date(string='Invoice Date',
                               readonly=True, states={'draft': [('readonly', False)],'proforma2': [('readonly', False)]}, index=True,
                               help="Keep empty to use the current date", copy=False)
    date_due = fields.Date(string='Due Date',
                           readonly=True, states={'draft': [('readonly', False)],'proforma2': [('readonly', False)]}, index=True, copy=False,
                           help="If you use payment terms, the due date will be computed automatically at the generation "
                                "of accounting entries. The payment term may compute several due dates, for example 50% "
                                "now and 50% in one month, but if you want to force a due date, make sure that the payment "
                                "term is not set on the invoice. If you keep the payment term and the due date empty, it "
                                "means direct payment.")

    payment_term = fields.Many2one('account.payment.term', string='Payment Terms',
                                   readonly=True, states={'draft': [('readonly', False)],'proforma2': [('readonly', False)]},
                                   help="If you use payment terms, the due date will be computed automatically at the generation "
                                        "of accounting entries. If you keep the payment term and the due date empty, it means direct payment. "
                                        "The payment term may compute several due dates, for example 50% now, 50% in one month.")


    invoice_line = fields.One2many('account.invoice.line', 'invoice_id', string='Invoice Lines',
                                   readonly=True, states={'draft': [('readonly', False)],'proforma2': [('readonly', False)]}, copy=True)
    tax_line = fields.One2many('account.invoice.tax', 'invoice_id', string='Tax Lines',
                               readonly=True, states={'draft': [('readonly', False)],'proforma2': [('readonly', False)]}, copy=True)





    check_total = fields.Float(string='Verification Total', digits=dp.get_precision('Account'),
                               readonly=True, states={'draft': [('readonly', False)],'proforma2': [('readonly', False)]}, default=0.0)


    partner_bank_id = fields.Many2one('res.partner.bank', string='Bank Account',
                                      help='Bank Account Number to which the invoice will be paid. A Company bank account if this is a Customer Invoice or Supplier Refund, otherwise a Partner bank account number.',
                                      readonly=True, states={'draft': [('readonly', False)],'proforma2': [('readonly', False)]})

    @api.multi
    def action_proforma_number(self):
        for inv in self:
            new_name = ''
            if not inv.internal_number:
                if inv.journal_id.sequence_id:
                    c = {'fiscalyear_id': inv.period_id.fiscalyear_id.id}
                    new_name = self.env['ir.sequence'].with_context(c).next_by_id(inv.journal_id.sequence_id.id)
            inv.write({'internal_number':new_name,'state':'proforma2'})
        return True


