# -*- coding: utf-8 -*-
{
    'name': "Custom Layouts",

    'summary': """
        Report Layout Customization""",

    'description': """
        Custom Reports
    """,

    'author': "Infintor",
    'website': "http://www.infintor.com",
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','account'],

    # always loaded
    'data': [
        
        'res_company.xml',
    ],
   
    'installable': True,
    'application': True,
    'auto_install': False,
}
