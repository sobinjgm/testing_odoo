# -*- coding:utf-8 -*-

{
    'name': 'Ancestry',
    'author': 'Digil',
    'website': 'www.odoo.com',
    'maintainer': 'Digil',
    'category': 'Partner',
    'version': '12.0.0.1',
    'licence': 'LGPL-V3',
    'summary':  """""",
    'description': """""",
    'depends': ['base', 'contacts',],
    'data': [
                'views/assets.xml',
                'views/res_partner.xml',
                'views/res_partner_kanban.xml',
                'security/ir.model.access.csv',
                'wizards/partner_relationship.xml',
            ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
