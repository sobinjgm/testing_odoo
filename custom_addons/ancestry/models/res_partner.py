# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from . import graph

class ResPartner(models.Model):
    _inherit = 'res.partner'
    
    company_type = fields.Selection(string='Company Type',
        selection=[('person', 'Individual'), ('company', 'Family')],
        compute='_compute_company_type', inverse='_write_company_type')
    is_house = fields.Boolean(string='House',copy=False, default=False)
    
    parent_parent_id = fields.Many2one('res.partner', related="parent_id.parent_id", string="Parent's Parent", help="Used in the kanban view")
    relationship = fields.Text()
    
    late = fields.Boolean(string='Late', copy=False, default=False)
    sex = fields.Selection([('male', 'Male'), ('female', 'Female'), ('other', 'Other')])
    gender = fields.Char(string='Gender')
    unclear_date = fields.Boolean(string="Unclarity in Dates")
    dob = fields.Datetime(string="Date of Birth")
    birth_order = fields.Integer(string="Birth Order")
    year_of_birth = fields.Char(string="Year of Birth")
    died_date = fields.Datetime(string="Died Date")
    year_of_death = fields.Char(string="Year of Death")
    cause_of_death = fields.Text(string="Cause of Death")
    age = fields.Integer(string='Age') 
    bachelor = fields.Boolean(string='Bachelor/Bachelorette', default=True)
    marital_status = fields.Selection([
                                        ('single', 'Single'),
                                        ('married', 'Married'),
                                        ('divorsed', 'Divorsed'),
                                        ('widow', 'Widow/Widower'),
                                        ('remarried', 'Remarried'),
                                      ], help='current marital position \n or marital position when deceased')
    spouse_ids = fields.One2many('res.partner.spouse.line', 'partner_id', string="Spouse(s)")
    
    life_partner_id = fields.Many2one('res.partner', string='Life Partner', help="Current or Last Life Partner")
    
    height = fields.Integer(string="Height")
    weight = fields.Float(string='Weight')
    hair_color = fields.Char(string="Hair Color")
    eye_color = fields.Char(string="Eye Color")
    face_shape = fields.Char(string="Face Shape")
    facial_hair = fields.Boolean(string="Facial Hair")
    bald = fields.Boolean()
    skin_color = fields.Char(string="SKin Color")
    show_full_features = fields.Boolean(string="Show all details")
    #introvert
    #date of visit
    #hair_type
    #religiosity
    #interested_in_topics (what excites you?)
    #scientific_temper
    #cesaerian
    #ivf
    #birth_weight
    #miscarriage
    #views on various topics like abortion, intercast marriages
    adopted = fields.Boolean()
    adopted_father_id = fields.Many2one('res.partner', string="Adopted Father")
    adopted_mother_id = fields.Many2one('res.partner', sting="Adopted Mother")
    
    house_locn_lattitude = fields.Char(string='Lattitude')
    house_locn_longitude = fields.Char(string='Longitude')
    
    father_id = fields.Many2one('res.partner', string='Father')
    mother_id = fields.Many2one('res.partner', string='Mother')
    
    children_ids = fields.Many2many('res.partner', 'parent_child_rel', 'children_ids', 'parents_ids', string='Children')
    parents_ids = fields.Many2many('res.partner', 'parent_child_rel', 'parents_ids', 'children_ids', string='parents')
    sibling_ids = fields.Many2many('res.partner', 'sib_sib_rel', 'sibling_ids', 'sibling_sec', string='Siblings')
    
    @api.multi
    def name_get(self):
        res = []
        for partner in self:
            name = partner._get_name()
            if ',' in name:
                first, sec = name.split(',')
                if partner.parent_id.parent_id:
                    name+= str(partner.parent_id.parent_id.name)
                    name = str(sec) + ', ' + str(partner.parent_id.parent_id.name) + ' ' + str(first)
                else:
                    name = str(sec) + ', ' + str(first)
            res.append((partner.id, name))
        return res
    
    @api.model
    def create(self, vals):
        parents_before_create = self.parents_ids
        res = super(ResPartner, self).create(vals)
        print("vals=",vals,"self",self,"res",res)
        if vals.get('father_id'):
            if vals.get('father_id') not in res.parents_ids.ids:
                res.update({'parents_ids': [(4,res.father_id.id)]})
            ft_other_children = res.mapped(lambda parent:parent.father_id.children_ids).filtered(lambda this: this.id != res.id)
            not_added_siblings = ft_other_children - res.sibling_ids
            res.update({'sibling_ids': [(4,sibling.id) for sibling in not_added_siblings]})
            for sib in not_added_siblings:
                sib.update({'sibling_ids': [(4,res.id)]})
        if vals.get('mother_id'):
            if vals.get('mother_id') not in res.parents_ids.ids:
                res.update({'parents_ids': [(4,res.mother_id.id)]})
            mt_other_children = res.mapped(lambda parent:parent.mother_id.children_ids).filtered(lambda this: this.id != res.id)
            not_added_siblings = mt_other_children - res.sibling_ids
            res.update({'sibling_ids': [(4,sibling.id) for sibling in not_added_siblings]})
            for sib in not_added_siblings:
                sib.update({'sibling_ids': [(4,res.id)]})
        if vals.get('children_ids'):
            child_ids = vals.get('children_ids')[0][2]
            child_rec_set = res.env['res.partner'].browse(child_ids)
            for person in child_rec_set:
                other_children = child_rec_set - person
                person_siblings = person.mapped(lambda sib: sib.sibling_ids)
                not_added_siblings = other_children - person_siblings
                person.update({'sibling_ids': [(4,sibling.id) for sibling in not_added_siblings]})
                if res.sex == 'male':
                    person.update({'father_id': res.id})
                if res.sex == 'female':
                    person.update({'mother_id': res.id})
        if vals.get('parents_ids'):
            parents_ids = vals.get('parents_ids')[0][2]
            if len(parents_ids) > 2:
                newly_entered_parent = res.env['res.partner'].browse(parents_ids) - parents_before_create
                old_mother = parents_before_create.filtered(lambda parent: parent.sex == 'female')
                new_mother = newly_entered_parent.filtered(lambda parent: parent.sex == 'female')
                old_father = parents_before_create.filtered(lambda parent: parent.sex == 'male')
                new_father = newly_entered_parent.filtered(lambda parent: parent.sex == 'male')
                if len(new_mother) == 1:
                    res.update({'parents_ids': [(3, old_mother.id)]})
                if len(new_father) == 1:
                    res.update({'parents_ids': [(3, old_father.id)]})
                if len(res.parents_ids) > 2:
                    raise ValidationError("Cannot have more than 2 parents")
            else:
                father = res.parents_ids.filtered(lambda parent: parent.sex == 'male')
                mother = res.parents_ids.filtered(lambda parent: parent.sex == 'female')
                if len(father) == 1:
                    res.update({'father_id': father.id})
                if len(mother) == 1:
                    res.update({'mother_id': mother.id})
        if vals.get('sibling_ids'):     #TODO on deletion delete the sibling from all other siblings too
            siblings_ids = vals.get('sibling_ids')[0][2]
            for partner in res.env['res.partner'].browse(siblings_ids):
                if res.id not in partner.sibling_ids.ids:
                    partner.update({'sibling_ids': [(4, res.id)]})
                for sibling in partner.mapped(lambda sib: sib.sibling_ids):
                    if sibling.id not in res.sibling_ids.ids and sibling.id != res.id:
                        res.update({'sibling_ids': [(4, sibling.id)]})
        if vals.get('life_partner_id'):
            if vals.get('life_partner_id') not in [spouse.life_partner_id.id for spouse in res.spouse_ids]:
                self.env['res.partner.spouse.line'].create({
                                                            'partner_id': res.id,
                                                            'life_partner_id': vals.get('life_partner_id'),
                                                            'spouse_order': len(res.spouse_ids)+1,
                                                            })
        return res
    
    @api.multi
    def write(self, vals):
        #TODO Other sexual orientations, adopted children, incest, deletion of relations, map
        parents_before_write = self.parents_ids
        res = super(ResPartner, self).write(vals)
        if vals.get('father_id'):
            if vals.get('father_id') not in self.parents_ids.ids:
                self.update({'parents_ids': [(4,self.father_id.id)]})
            ft_other_children = self.mapped(lambda parent:parent.father_id.children_ids).filtered(lambda this: this.id != self.id)
            not_added_siblings = ft_other_children - self.sibling_ids
            self.update({'sibling_ids': [(4,sibling.id) for sibling in not_added_siblings]})
            for sib in not_added_siblings:
                sib.update({'sibling_ids': [(4,self.id)]})
        if vals.get('mother_id'):
            if vals.get('mother_id') not in self.parents_ids.ids:
                self.update({'parents_ids': [(4,self.mother_id.id)]})
            mt_other_children = self.mapped(lambda parent:parent.mother_id.children_ids).filtered(lambda this: this.id != self.id)
            not_added_siblings = mt_other_children - self.sibling_ids
            self.update({'sibling_ids': [(4,sibling.id) for sibling in not_added_siblings]})
            for sib in not_added_siblings:
                sib.update({'sibling_ids': [(4,self.id)]})
        if vals.get('children_ids'):
            child_ids = vals.get('children_ids')[0][2]
            child_rec_set = self.env['res.partner'].browse(child_ids)
            for person in child_rec_set:
                other_children = child_rec_set - person
                person_siblings = person.mapped(lambda sib: sib.sibling_ids)
                not_added_siblings = other_children - person_siblings
                person.update({'sibling_ids': [(4,sibling.id) for sibling in not_added_siblings]})
                if self.sex == 'male':
                    person.update({'father_id': self.id})
                if self.sex == 'female':
                    person.update({'mother_id': self.id})
        if vals.get('parents_ids'):
            parents_ids = vals.get('parents_ids')[0][2]
            if len(parents_ids) > 2:
                newly_entered_parent = self.env['res.partner'].browse(parents_ids) - parents_before_write
                old_mother = parents_before_write.filtered(lambda parent: parent.sex == 'female')
                new_mother = newly_entered_parent.filtered(lambda parent: parent.sex == 'female')
                old_father = parents_before_write.filtered(lambda parent: parent.sex == 'male')
                new_father = newly_entered_parent.filtered(lambda parent: parent.sex == 'male')
                if len(new_mother) == 1:
                    self.update({'parents_ids': [(3, old_mother.id)]})
                if len(new_father) == 1:
                    self.update({'parents_ids': [(3, old_father.id)]})
                if len(self.parents_ids) > 2:
                    raise ValidationError("Cannot have more than 2 parents")
            else:
                father = self.parents_ids.filtered(lambda parent: parent.sex == 'male')
                mother = self.parents_ids.filtered(lambda parent: parent.sex == 'female')
                if len(father) == 1:
                    self.update({'father_id': father.id})
                if len(mother) == 1:
                    self.update({'mother_id': mother.id})
        if vals.get('sibling_ids'):     #TODO on deletion delete the sibling from all other siblings too
            siblings_ids = vals.get('sibling_ids')[0][2]
            for partner in self.env['res.partner'].browse(siblings_ids):
                if self.id not in partner.sibling_ids.ids:
                    partner.update({'sibling_ids': [(4, self.id)]})
                for sibling in partner.mapped(lambda sib: sib.sibling_ids):
                    if sibling.id not in self.sibling_ids.ids and sibling.id != self.id:
                        self.update({'sibling_ids': [(4, sibling.id)]})
        if vals.get('life_partner_id'):
            if vals.get('life_partner_id') not in [spouse.life_partner_id.id for spouse in self.spouse_ids]:
                self.env['res.partner.spouse.line'].create({
                                                            'partner_id': self.id,
                                                            'life_partner_id': vals.get('life_partner_id'),
                                                            'spouse_order': len(self.spouse_ids)+1,
                                                            })
        return res
    
    @api.onchange('marital_status')
    def _onchange_marital_status(self):
        for record in self:
            if record.marital_status == False or record.marital_status == 'single':
                record.bachelor = True
            else:
                record.bachelor = False

    @api.constrains('sibling_ids')
    def _check_self_not_sibling(self):
        for record in self:
            if record.id in record.sibling_ids.ids:
                raise ValidationError("Self given in siblings")
            
    @api.constrains('father_id', 'mother_id')
    def _check_parent_sex(self):
        for record in self:
            if record.father_id.sex == 'female':
                raise ValidationError("Sex of Father choosen as Female")
            if record.mother_id.sex == 'male':
                raise ValidationError("sex of Mother choosen as Male")
            
            
    @api.multi
    def button_function(self):
        for record in self:
            smallest_path = record.compute_relationship_path()
            relationship = record._compute_relationship(smallest_path[0])
            record.write({'relationship':relationship})
            print("relationship", relationship)
            relation_view = self.env.ref('ancestry.relation_wizard_view')
            return {
                'name': _('Relationship'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'partner.relationship',
                'views': [(relation_view.id, 'form')],
                'view_id': relation_view.id,
                'target': 'new',
            }
    
    @api.multi
    def _compute_relation_dictionary(self):
        all_partners = self.search([('is_company', '=', False)])
        relation_dict = {}
        for partner in all_partners:
            if str(partner.id) not in relation_dict:
                relation_dict[str(partner.id)] = []
            for parent in partner.parents_ids:
                if str(parent.id) not in relation_dict:
                    relation_dict[str(parent.id)] = []
                    relation_dict[str(partner.id)].append(str(parent.id))
                    relation_dict[str(parent.id)].append(str(partner.id))
                else:
                    relation_dict[str(partner.id)].append(str(parent.id))
                    relation_dict[str(parent.id)].append(str(partner.id))
            for spouse in partner.spouse_ids:
                if str(spouse.life_partner_id.id) not in relation_dict:
                    relation_dict[str(spouse.life_partner_id.id)] = []
                    relation_dict[str(partner.id)].append(str(spouse.life_partner_id.id))
                    relation_dict[str(spouse.life_partner_id.id)].append(str(partner.id))
                else:
                    relation_dict[str(partner.id)].append(str(spouse.life_partner_id.id))
                    relation_dict[str(spouse.life_partner_id.id)].append(str(partner.id))
        for vertex in relation_dict:
            relation_dict[vertex] = list(set(relation_dict[vertex]))
        print("relation_dict", relation_dict)
        return relation_dict
    
    @api.multi
    def compute_relationship_path(self, first_partner_id=None, second_partner_id=None):
        relation_dict = self._compute_relation_dictionary()
        graph_object = graph.Graph(relation_dict)

        if not first_partner_id and not second_partner_id:
            current_user_partner = self.env['res.users'].browse(self._context.get('uid')).partner_id.id
            current_brw_partner = self._context.get('active_id', False)
            print("current_user_partner",current_user_partner)
            print("current_brw_partner",current_brw_partner)
            if current_user_partner and current_brw_partner:
                smallest_path = graph_object.find_smallest_path(str(current_user_partner), str(current_brw_partner))
                print("rrr", smallest_path)
        else:
            smallest_path = graph_object.find_smallest_path(str(first_partner_id), str(second_partner_id))
        return smallest_path
        
    
    @api.multi
    def _compute_relationship(self, smallest_path=[]):
        if len(smallest_path) >= 1:
            relationship = str("{}".format(self.env['res.partner'].browse(int(smallest_path[0])).name))
            for pos, vertex in enumerate(smallest_path):
                if not pos == len(smallest_path)-1:
                    if self._check_if_parent(vertex, smallest_path[pos+1]):    #second parent of first
                        relationship = self._get_parent_relation(vertex, smallest_path[pos+1], relationship)
                    if self._check_if_child(vertex, smallest_path[pos+1]):    #second child of first
                        relationship = self._get_child_relation(vertex, smallest_path[pos+1], relationship)
                    if self._check_if_spouse(vertex, smallest_path[pos+1]):    
                        relationship = self._get_marriage_relation(vertex, smallest_path[pos+1], relationship)
    #                self._check_if_sibling(vertex, smallest_path[pos+1])
            return str(relationship)
        return "No Relation"
        
    def _check_if_parent(self, first_partner=None, second_partner=None):
        if self.env['res.partner'].browse(int(second_partner)) in self.env['res.partner'].browse(int(first_partner)).parents_ids:
            return True
        else:
            return False

    def _get_parent_relation(self, first_partner=None, second_partner=None, relationship=""):
        first_partner = self.env['res.partner'].browse(int(first_partner))
        second_partner = self.env['res.partner'].browse(int(second_partner))
        if first_partner.father_id == second_partner:
            relationship += "'s Father " + str(second_partner.name)
        elif first_partner.mother_id == second_partner:
            relationship += "'s Mother " + str(second_partner.name)
        return str(relationship)
        
        
    def _check_if_child(self, first_partner=None, second_partner=None):
        if self.env['res.partner'].browse(int(second_partner)) in \
                                                                self.env['res.partner'].browse(int(first_partner)).children_ids:
            return True
        else:
            return False
        
    def _get_child_relation(self, first_partner=None, second_partner=None, relationship=""):
        first_partner = self.env['res.partner'].browse(int(first_partner))
        second_partner = self.env['res.partner'].browse(int(second_partner))
        if second_partner in first_partner.children_ids:
            relationship += "'s " + str(self._calculate_birth_order(first_partner, second_partner)) + str(second_partner.name)
        return str(relationship)
    
    
    def _calculate_birth_order(self, parent=None, child=None):
        if len(parent.children_ids) == 1:
            if child.sex == 'male':
                return "Only Son "
            elif child.sex == 'female':
                return "Only Daughter "
            else:
                return "Only Child "
        else:
#            order = 1
#            ordered_children = parent.children_ids.sorted(key=lambda child: child.birth_order) #TODO group by sex
            if child.birth_order == 1:
                return "1st" + " Child "
            elif child.birth_order == 2:
                return "2nd" + " Child "
            elif child.birth_order == 3:
                return "3rd" + " Child "
            else:
                return str(child.birth_order) + "th" + " Child "

    def _check_if_spouse(self, first_partner=None, second_partner=None):
        first_partner = self.env['res.partner'].browse(int(first_partner))
        second_partner = self.env['res.partner'].browse(int(second_partner))
        if second_partner in \
                        [spouse.life_partner_id for spouse in first_partner.spouse_ids]:
            return True
        else:
            return False
    
    def _get_marriage_relation(self, first_partner=None, second_partner=None, relationship=""):
        first_partner = self.env['res.partner'].browse(int(first_partner))
        second_partner = self.env['res.partner'].browse(int(second_partner))
        if second_partner.sex == 'male':
            relationship += "'s" + str(self._calculate_spouse_status(first_partner, second_partner)) + "Husband " + str(second_partner.name)
        elif second_partner.sex == 'female':
            relationship += "'s" + str(self._calculate_spouse_status(first_partner, second_partner)) + "Wife " + str(second_partner.name)
        else:
            relationship += "'s" + str(self._calculate_spouse_status(first_partner, second_partner)) + "LifePartner " + str(second_partner.name)
        return relationship
        
    def _calculate_spouse_status(self, first_partner=None, second_partner=None):
        if len(first_partner.spouse_ids) == 1 and first_partner.life_partner_id == first_partner.spouse_ids.life_partner_id:
            return " "
        else:
            for spouse in first_partner.spouse_ids:
                if spouse.life_partner_id == second_partner:
                    if spouse.spouse_order == 1:
                        return " 1st "
                    elif spouse.spouse_order == 2:
                        return " 2nd "
                    elif spouse.spouse_order == 3:
                        return " 3rd "
                    elif spouse.spouse_order > 3:
                        return " " + str(spouse.spouse_order) + "th "
        
ResPartner()


class ResPartnerSpouseLine(models.Model):
    _name='res.partner.spouse.line'
    
    partner_id = fields.Many2one('res.partner')
    life_partner_id = fields.Many2one('res.partner', string="Life Partner")
    spouse_order = fields.Integer(string="Spouse Order", default=1)
    divorsed = fields.Boolean(string="Issue")
    marriage_date = fields.Date(string="Marriage Date")
    divorsed_date = fields.Date(string="Divorsed_date")
    illegal_marriage = fields.Boolean(string="Illegal")
    image_small = fields.Binary(related="life_partner_id.image_small")
    parent_id = fields.Many2one('res.partner', related="life_partner_id.parent_id")
    parent_parent_id = fields.Many2one('res.partner', related="life_partner_id.parent_parent_id")
    is_company = fields.Boolean(related="life_partner_id.is_company")
    display_name = fields.Char(related="life_partner_id.display_name")
    
    @api.model
    def create(self, vals):
        res = super(ResPartnerSpouseLine, self).create(vals)
        if res.partner_id.id not in [spouse.life_partner_id.id for spouse in res.life_partner_id.spouse_ids] or len(res.life_partner_id.spouse_ids) == 0:
            inverse_life_partner = self.env['res.partner.spouse.line'].create({
                                    'partner_id': res.life_partner_id.id,
                                    'life_partner_id': res.partner_id.id,
                                    'spouse_order': len(res.life_partner_id.spouse_ids) + 1,
                                    })
            res.life_partner_id.update({'bachelor': False})
            if res.life_partner_id.life_partner_id.id == False:
                res.life_partner_id.life_partner_id = res.partner_id.id
            if not res.partner_id.life_partner_id.id:
                res.partner_id.life_partner_id = res.life_partner_id.id
            partner_children = res.life_partner_id.children_ids
            for own_child in res.partner_id.children_ids:
                for partner_child in partner_children:
                    if partner_child != own_child:
                        own_child.update({'sibling_ids': [(4, partner_child.id)]})
        return res
    
    
    @api.multi
    def unlink(self):
        inverse_to_be_deleted = self.life_partner_id.spouse_ids.search([('partner_id', '=', self.life_partner_id.id), ('life_partner_id', '=', self.partner_id.id)])
        partner_children = self.life_partner_id.children_ids    #FIXME not working!!
        for own_child in self.partner_id.children_ids:
            for partner_child in partner_children:
                if partner_child != own_child:
                    own_child.update({'sibling_ids': [(2, partner_child.id)]})
        if self.life_partner_id == self.partner_id.life_partner_id and len(self.partner_id.spouse_ids) == 1: #FIXME issue here
            self.partner_id.update({'life_partner_id': False,
                                    'bachelor': True, 
                                    'marital_status': False,
                                    })
        res = super(ResPartnerSpouseLine, self).unlink()
        for record in inverse_to_be_deleted:
            if record.partner_id.life_partner_id == record.life_partner_id and len(record.partner_id.spouse_ids) == 1:
                record.partner_id.update({'life_partner_id': False, 
                                          'bachelor': True, 
                                          'marital_status': False,
                                          })
            record.unlink()
#        input("eeeeeeeeeeeeeee")
        return res

ResPartnerSpouseLine()

















