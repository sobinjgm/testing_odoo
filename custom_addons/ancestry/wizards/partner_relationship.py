# -*- coding:utf-8 -*-
from odoo import models, fields, api, _

class PartnerRelationship(models.TransientModel):
    _name = 'partner.relationship'
    _description = 'Partner relationship'
    
    relationship = fields.Text()
    see_other_relationship = fields.Boolean(string="Check Other Relations")
    partner_1 = fields.Many2one('res.partner', string="Partner 1")
    partner_2 = fields.Many2one('res.partner', string="Partner 2")
    calculated_relation = fields.Text(compute="_calculate_relation")
    calculated_inverse_relation = fields.Text()
    
    @api.model
    def default_get(self, fields):
        res = super(PartnerRelationship, self).default_get(fields)
        active_brw = self.env[self.env.context.get('active_model', 'res.partner')].browse(self.env.context.get('active_id', []))
        if active_brw:
            res.update({
                        'relationship': active_brw.relationship,
                        })
        return res
     
    @api.depends('partner_1', 'partner_2')   
    @api.multi
    def _calculate_relation(self):
        for record in self:
            if record.partner_1 and record.partner_2:
                smallest_path = self.env['res.partner'].compute_relationship_path(record.partner_1.id, record.partner_2.id)
                relationship = self.env['res.partner']._compute_relationship(smallest_path[0])
                
                inverse_path = self.env['res.partner'].compute_relationship_path(record.partner_2.id, record.partner_1.id)
                inverse_relationship = self.env['res.partner']._compute_relationship(inverse_path[0])
                
                record.update({'calculated_relation': relationship,
                                'calculated_inverse_relation': inverse_relationship,
                              })
                print("relationship", relationship)
            
            
            
            
            
            
            
