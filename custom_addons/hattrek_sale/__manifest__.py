# -*- coding: utf-8 -*-

{
    'name': 'Hattrek Sale',
    'sequence': 10,
    'version': '12.0.10',
    'author': "Infintor",
    'website': "http://www.infintor.com",
    'depends': ['sale'],
    'category': 'Sales',
    'description': """
        Hattrek Sale Extension
    """,
    'data': [
        'views/product_attribute_views.xml',        
        'views/sale_order_views.xml',        
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
