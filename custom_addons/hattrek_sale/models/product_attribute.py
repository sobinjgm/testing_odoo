# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class ProductAttribute(models.Model):
    _inherit = "product.attribute"

    hattrek_attribute = fields.Selection([
        ('color', 'Color'),
        ('size', 'Size')], string="Hattrek Attribute")


ProductAttribute()
