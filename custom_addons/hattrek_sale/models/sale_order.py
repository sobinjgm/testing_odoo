# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    product_tmpl_id = fields.Many2one('product.template', string="Product")
    hattrek_attrib_color = fields.Many2one(
        "product.attribute.value", 
        domain=[('attribute_id.hattrek_attribute', '=', 'color')], 
        string="Colors")
    hattrek_attrib_size = fields.Many2one(
        "product.attribute.value", 
        domain=[('attribute_id.hattrek_attribute', '=', 'size')], 
        string="Size")

    @api.onchange('product_tmpl_id')
    def _onchange_product_tmpl_id(self):
        self.product_id = self.product_tmpl_id and self.product_tmpl_id.product_variant_id or False
        self.hattrek_attrib_color = False
        self.hattrek_attrib_size = False

    @api.onchange('hattrek_attrib_color', 'hattrek_attrib_size')
    def _onchange_hattrek_attributes(self):
        if self.product_tmpl_id and self.hattrek_attrib_color and self.hattrek_attrib_color:
            color_id = self.hattrek_attrib_color.id
            size_id = self.hattrek_attrib_size.id
            product = self.product_tmpl_id.product_variant_ids.filtered(lambda rec: 
                color_id in rec.attribute_value_ids.ids and size_id in rec.attribute_value_ids.ids)
            self.product_id = product and product.id
        

SaleOrderLine()    
