{
    'name': 'Invoice Print Format',
    'version': '1.0',
    'category': 'Accounting',
    "sequence": 8,
    'summary': 'Customized Invoice General Format',
    'complexity': "User Friendly",
    'description': """
            This module add the general invoice format.
    """,
    'author': 'Odoo',
    'images': [],
    'depends': ['account'],
    'init_xml': [],
    'data': [
        'report/report.xml',
        'report/report_invoice_print.xml',
    ],
    'css': [],
    'qweb': [],
    'js': [], 
    'images': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}

