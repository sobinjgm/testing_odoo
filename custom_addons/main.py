# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import xmlrpc.client
from odoo import http





class SpaDetails(http.Controller):

    @http.route('/web/spa/servicecategories', type='json', auth="none")
    def get_spa_service_categories(self):
        username = "spa"
        pwd = "spa@123"
        dbname = "SECHOIR_TESTING"
        sock_common = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/common")
        uid = sock_common.authenticate(dbname, username, pwd,{})
        sock = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/object")
        result =sock.execute_kw(dbname, uid, pwd, 'res.users', 'get_spa_service_categories', [[]])
        return result


    @http.route('/web/spa/services', type='json', auth="none")
    def get_spa_services(self,category_id):
        username = "spa"
        pwd = "spa@123"
        dbname = "SECHOIR_TESTING"
        sock_common = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/common")
        uid = sock_common.authenticate(dbname, username, pwd,{})
        sock = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/object")
        result = sock.execute_kw(dbname, uid, pwd, 'res.users', 'get_spa_services', ["",category_id])
        return result


    @http.route('/web/spa/servicedetails', type='json', auth="none")
    def get_spa_service_details(self,service_id):
        username = "spa"
        pwd = "spa@123"
        dbname = "SECHOIR_TESTING"
        sock_common = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/common")
        uid = sock_common.authenticate(dbname, username, pwd,{})
        sock = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/object")
        result = sock.execute_kw(dbname, uid, pwd, 'res.users', 'get_spa_service_details', ["",service_id])
        return result

    @http.route('/web/spa/usercreation', type='json', auth="none")
    def create_spa_user(self, name,login,password,mobile):
        username = "spa"
        pwd = "spa@123"
        dbname = "SECHOIR_TESTING"
        sock_common = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/common")
        uid = sock_common.authenticate(dbname, username, pwd,{})
        sock = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/object")
        result = sock.execute_kw(dbname, uid, pwd, 'res.users', 'create_spa_user', ["", name,login,password,mobile])
        return result

    @http.route('/web/spa/getbeauticianlist', type='json', auth="none")
    def get_spa_beautician_list(self,  service_id,reservation_date,today_reservation):
        username = "spa"
        pwd = "spa@123"
        dbname = "SECHOIR_TESTING"
        sock_common = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/common")
        uid = sock_common.authenticate(dbname, username, pwd,{})
        sock = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/object")
        result = sock.execute_kw(dbname, uid, pwd, 'res.users', 'get_beautician_list', ["",  service_id,reservation_date,today_reservation])
        return result

    @http.route('/web/spa/getbeauticianslots', type='json', auth="none")
    def get_spa_beautician_slots(self,service_id, reservation_date,employee_id,today_reservation):
        username = "spa"
        pwd = "spa@123"
        dbname = "SECHOIR_TESTING"
        sock_common = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/common")
        uid = sock_common.authenticate(dbname, username, pwd,{})
        sock = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/object")
        result = sock.execute_kw(dbname, uid, pwd, 'res.users', 'get_beautician_slots', ["", service_id, reservation_date,employee_id,today_reservation])
        return result

    @http.route('/web/spa/createspabooking', type='json', auth="none")
    def create_spa_booking(self,service_id,reservation_date,employee_id,user_id,start_time,source):
        username = "spa"
        pwd = "spa@123"
        dbname = "SECHOIR_TESTING"
        sock_common = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/common")
        uid = sock_common.authenticate(dbname, username, pwd,{})
        sock = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/object")
        result = sock.execute_kw(dbname, uid, pwd, 'res.users', 'create_spa_booking', ["",service_id,reservation_date,employee_id,user_id,start_time,source])
        return result

    @http.route('/web/spa/makebookingpaid', type='json', auth="none")
    def make_spa_booking_paid(self, booking_id):
        username = "spa"
        pwd = "spa@123"
        dbname = "SECHOIR_TESTING"
        sock_common = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/common")
        uid = sock_common.authenticate(dbname, username, pwd, {})
        sock = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/object")
        result = sock.execute_kw(dbname, uid, pwd, 'res.users', 'make_spa_booking_paid', ["", booking_id])
        return result


    @http.route('/web/spa/homeservices', type='json', auth="none")
    def get_spa_services_home(self,category_id):
        username = "spa"
        pwd = "spa@123"
        dbname = "SECHOIR_TESTING"
        sock_common = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/common")
        uid = sock_common.authenticate(dbname, username, pwd,{})
        sock = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/object")
        result = sock.execute_kw(dbname, uid, pwd, 'res.users', 'get_spa_services_home', ["",category_id])
        return result

    @http.route('/web/spa/getbeauticianlisthome', type='json', auth="none")
    def get_beautician_list_home(self, service_id, reservation_date, today_reservation):
        username = "spa"
        pwd = "spa@123"
        dbname = "SECHOIR_TESTING"
        sock_common = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/common")
        uid = sock_common.authenticate(dbname, username, pwd, {})
        sock = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/object")
        result = sock.execute_kw(dbname, uid, pwd, 'res.users', 'get_beautician_list_home',
                                 ["", service_id, reservation_date, today_reservation])
        return result

    @http.route('/web/spa/getbeauticianslotshome', type='json', auth="none")
    def get_beautician_slots_home(self, service_id, reservation_date, employee_id, today_reservation):
        username = "spa"
        pwd = "spa@123"
        dbname = "SECHOIR_TESTING"
        sock_common = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/common")
        uid = sock_common.authenticate(dbname, username, pwd, {})
        sock = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/object")
        result = sock.execute_kw(dbname, uid, pwd, 'res.users', 'get_beautician_slots_home',
                                 ["", service_id, reservation_date, employee_id, today_reservation])
        return result

    @http.route('/web/spa/createspabookinghome', type='json', auth="none")
    def create_spa_booking_home(self, service_id, reservation_date, employee_id, user_id, start_time, source):
        username = "spa"
        pwd = "spa@123"
        dbname = "SECHOIR_TESTING"
        sock_common = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/common")
        uid = sock_common.authenticate(dbname, username, pwd, {})
        sock = xmlrpc.client.ServerProxy("http://139.59.43.193:8055/xmlrpc/object")
        result = sock.execute_kw(dbname, uid, pwd, 'res.users', 'create_spa_booking_home',
                                 ["", service_id, reservation_date, employee_id, user_id, start_time, source])
        return result