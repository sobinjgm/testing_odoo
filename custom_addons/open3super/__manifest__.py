# -*- coding: utf-8 -*-
{
    'name': "xxx",

    'summary': """
        Report Layout Customization""",

    'description': """
        Custom Reports
    """,

    'author': "Infintor",
    'website': "http://www.infintor.com",
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        
        'super1.xml',
        'ir.model.access.csv',
    ],
   
    'installable': True,
    'application': True,
    'auto_install': False,
}
