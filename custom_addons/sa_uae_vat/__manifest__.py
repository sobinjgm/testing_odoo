# -*- coding: utf-8 -*-
# Copyright (C) 2017-Today  Technaureus Info Solutions(<http://technaureus.com/>).
{
    'name': 'Saudi/UAE VAT(Arabic VAT)',
    'version': '1.0',
    'sequence': 1,
    'category': 'Localization',
    'summary': 'SA UAE VAT configuration',
    'author': 'Technaureus Info Solutions',
    'website': 'http://www.technaureus.com/',
    'price': 20,
    'currency': 'EUR',
    'license': 'Other proprietary',
    'description': """
This module is for configure of Saudi and UAE VAT
        """,
    'depends': ['account','product'],
    'data': [
        'views/product_view.xml',
        'views/res_company_view.xml',
        'views/res_partner_view.xml'
    ],
    'images': ['images/main_screenshot.png'],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
