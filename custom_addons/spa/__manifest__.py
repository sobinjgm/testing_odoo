# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Spa Management Base',
    'version': '10.0.1.0.0',
    'author': 'Infintor',
    'category': 'Generic Modules/Spa Management',
    'license': 'AGPL-3',
    'depends': ['sale_stock','hr','project'],
    'data': [
            'security/spa_security.xml',
            'security/ir.model.access.csv',
            'views/spa_sequence.xml',
            'views/spa_report.xml',
            'views/report_spa_management.xml',
            'views/spa_view.xml',
            'wizard/spa_wizard.xml',
    ],
    'css': ['static/src/css/employee_kanban.css'],
    'installable': True,
    'application': True,
}
