# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

import time
import datetime
from odoo.exceptions import ValidationError, UserError
from odoo.osv import expression
from odoo.tools import misc, DEFAULT_SERVER_DATETIME_FORMAT
from odoo import models, fields, api, _
from decimal import Decimal


def _offset_format_timestamp1(src_tstamp_str, src_format, dst_format,
                              ignore_unparsable_time=True, context=None):
    """
    Convert a source timeStamp string into a destination timeStamp string,
    attempting to apply the correct offset if both the server and local
    timeZone are recognized,or no offset at all if they aren't or if
    tz_offset is false (i.e. assuming they are both in the same TZ).

    @param src_tstamp_str: the STR value containing the timeStamp.
    @param src_format: the format to use when parsing the local timeStamp.
    @param dst_format: the format to use when formatting the resulting
     timeStamp.
    @param server_to_client: specify timeZone offset direction (server=src
                             and client=dest if True, or client=src and
                             server=dest if False)
    @param ignore_unparsable_time: if True, return False if src_tstamp_str
                                   cannot be parsed using src_format or
                                   formatted using dst_format.
    @return: destination formatted timestamp, expressed in the destination
             timezone if possible and if tz_offset is true, or src_tstamp_str
             if timezone offset could not be determined.
    """
    if not src_tstamp_str:
        return False
    res = src_tstamp_str
    if src_format and dst_format:
        try:
            # dt_value needs to be a datetime.datetime object
            # (so notime.struct_time or mx.DateTime.DateTime here!)
            dt_value = datetime.datetime.strptime(src_tstamp_str, src_format)
            if context.get('tz', False):
                try:
                    import pytz
                    src_tz = pytz.timezone(context['tz'])
                    dst_tz = pytz.timezone('UTC')
                    src_dt = src_tz.localize(dt_value, is_dst=True)
                    dt_value = src_dt.astimezone(dst_tz)
                except Exception:
                    pass
            res = dt_value.strftime(dst_format)
        except Exception:
            # Normal ways to end up here are if strptime or strftime failed
            if not ignore_unparsable_time:
                return False
            pass
    return res

class ProductProduct(models.Model):

    _inherit = "product.product"

    iscategid = fields.Boolean('Is categ id')
    isservice = fields.Boolean('Is Service id')


class HrEmployee(models.Model):

    _inherit = "hr.employee"

    isemployee = fields.Boolean('Is Employee')

class SpaEmployee(models.Model):

    _name = 'spa.employee'
    _description = 'Spa Employee'

    employee_id = fields.Many2one('hr.employee', 'Employee_id',
                                 required=True, delegate=True,
                                 ondelete='cascade')
    floor_id = fields.Many2one('spa.floor', 'Floor No',
                               help='At which floor the employee is working.')

    status = fields.Selection([('available', 'Available'),
                               ('occupied', 'Occupied')],
                              'Status', default='available')
    spa_service_line_ids = fields.One2many('spa.service.line', 'employee_id',
                                           string='Service Reservation Line')
    allowed_services = fields.Many2many('spa.services', 'emp_allowed_service_rel', 'employee_id', 'service_id',
                                        string="Allowed Services", copy=False)
    


    def onchange_department_id(self, cr, uid, ids, department_id, context=None):
        value = {'parent_id': False}
        if department_id:
            department = self.pool.get('hr.department').browse(cr, uid, department_id)
            value['parent_id'] = department.manager_id.id
        return {'value': value}

    def onchange_user(self, cr, uid, ids, user_id, context=None):
        work_email = False
        if user_id:
            work_email = self.pool.get('res.users').browse(cr, uid, user_id, context=context).email
        return {'value': {'work_email': work_email}}

    @api.onchange('isemployee')
    def isemployee_change(self):
        '''
        Based on isemployee, status will be updated.
        ----------------------------------------
        @param self: object pointer
        '''
        if self.isemployee is False:
            self.status = 'occupied'
        if self.isemployee is True:
            self.status = 'available'

    @api.multi
    def write(self, vals):
        """
        Overrides orm write method.
        @param self: The object pointer
        @param vals: dictionary of fields value.
        """
        if 'isemployee' in vals and vals['isemployee'] is False:
            vals.update({'color': 2, 'status': 'occupied'})
        if 'isemployee' in vals and vals['isemployee'] is True:
            vals.update({'color': 5, 'status': 'available'})
        ret_val = super(SpaEmployee, self).write(vals)
        return ret_val

    @api.multi
    def set_employee_status_occupied(self):
        """
        This method is used to change the state
        to occupied of the employee.
        ---------------------------------------
        @param self: object pointer
        """
        return self.write({'isemployee': False, 'color': 2})

    @api.multi
    def set_employee_status_available(self):
        """
        This method is used to change the state
        to available of the employee.
        ---------------------------------------
        @param self: object pointer
        """
        return self.write({'isemployee': True, 'color': 5})


class SpaFolio(models.Model):

    @api.multi
    def name_get(self):
        res = []
        fname = ''
        for rec in self:
            if rec.order_id:
                fname = str(rec.name)
                res.append((rec.id, fname))
        return res

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if args is None:
            args = []
        args += ([('name', operator, name)])
        folio = self.search(args, limit=100)
        return folio.name_get()

    @api.model
    def _needaction_count(self, domain=None):
        """
         Show a count of draft state folio on the menu badge.
         @param self: object pointer
        """
        return self.search_count([('state', '=', 'draft')])


    @api.model
    def _get_start_time(self):
        if self._context.get('tz'):
            to_zone = self._context.get('tz')
        else:
            to_zone = 'UTC'
        return _offset_format_timestamp1(time.strftime("%Y-%m-%d 12:00:00"),
                                         '%Y-%m-%d %H:%M:%S',
                                         '%Y-%m-%d %H:%M:%S',
                                         ignore_unparsable_time=True,
                                         context={'tz': to_zone})

    @api.model
    def _get_end_time(self):
        if self._context.get('tz'):
            to_zone = self._context.get('tz')
        else:
            to_zone = 'UTC'
        tm_delta = datetime.timedelta(days=1)
        return datetime.datetime.strptime(_offset_format_timestamp1
                                          (time.strftime("%Y-%m-%d 12:00:00"),
                                           '%Y-%m-%d %H:%M:%S',
                                           '%Y-%m-%d %H:%M:%S',
                                           ignore_unparsable_time=True,
                                           context={'tz': to_zone}),
                                          '%Y-%m-%d %H:%M:%S') + tm_delta

    @api.multi
    def copy(self, default=None):
        '''
        @param self: object pointer
        @param default: dict of default values to be set
        '''
        res = {}
        try:
            res = super(SpaFolio, self).copy(default=default)
        except Exception:
            raise UserError(_('You can not copy this folio'))
            #        return self.env['sale.order'].copy(default=default)
        return res

    _name = 'spa.folio'
    _description = 'spa folio new'
    _rec_name = 'order_id'
    _order = 'id'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']


    name = fields.Char('Folio Number', readonly=True, index=True,
                       default='New')
    order_id = fields.Many2one('sale.order', 'Order', delegate=True,
                               required=True, ondelete='cascade')

    service_lines = fields.One2many('spa.service.line', 'folio_id',
                                    readonly=True,
                                    states={'draft': [('readonly', False)],
                                            'sent': [('readonly', False)]},
                                    help="Spa services detail provide to"
                                         "customer and it will include in "
                                         "main Invoice.")

    spa_policy = fields.Selection([('prepaid', 'On Booking'),
                                   ('manual', 'On Check In'),
                                   ('picking', 'On Checkout')],
                                  'Spa Policy', default='manual',
                                  help="Spa policy for payment that "
                                       "either the guest has to payment at "
                                       "booking time or check-in "
                                       "check-out time.")
    duration = fields.Char('Duration in Hours',
                           help="Duration which will automatically "
                                "count from the start time and end time. ")

    spa_invoice_id = fields.Many2one('account.invoice', 'Invoice')

    booking_flag = fields.Boolean('Booking Flag')

    @api.constrains('start_time', 'end_time')
    def check_time(self):
        '''
        This method is used to validate the start_time and end_time.
        -------------------------------------------------------------------
        @param self: object pointer
        @return: raise warning depending on the validation
        '''
        if self.start_time and self.end_time:
            if self.start_time >= self.end_time:
                raise ValidationError(_('Start time should be \
                less than the End time!'))



    @api.model
    def create(self, vals):
        """
        Overrides orm create method.
        @param self: The object pointer
        @param vals: dictionary of fields value.
        @return: new record set for spa folio.
        """
        if not vals:
            vals = {}
        vals['name'] = self.env['ir.sequence'].get('spa.folio')
        folio_id = super(SpaFolio, self).create(vals)
        return folio_id


    @api.onchange('warehouse_id')
    def onchange_warehouse_id(self):
        '''
        When you change warehouse it will update the warehouse of
        the hotel folio as well
        ----------------------------------------------------------
        @param self: object pointer
        '''
        return self.order_id._onchange_warehouse_id()


    @api.onchange('partner_id')
    def onchange_partner_id(self):
        '''
        When you change partner_id it will update the partner_invoice_id,
        partner_shipping_id and pricelist_id of the spa folio as well
        ---------------------------------------------------------------
        @param self: object pointer
        '''
        if self.partner_id:
            partner_rec = self.env['res.partner'].browse(self.partner_id.id)
            order_ids = [folio.order_id.id for folio in self]
            if not order_ids:
                self.partner_invoice_id = partner_rec.id
                self.partner_shipping_id = partner_rec.id
                self.pricelist_id = partner_rec.property_product_pricelist.id
                raise UserError(_('Not Any Order For  %s ' % (partner_rec.name)))
            else:
                self.partner_invoice_id = partner_rec.id
                self.partner_shipping_id = partner_rec.id
                self.pricelist_id = partner_rec.property_product_pricelist.id



    @api.multi
    def button_dummy(self):
        '''
        @param self: object pointer
        '''
        for folio in self:
            order = folio.order_id
            x = order.button_dummy()
        return x

    @api.multi
    def action_done(self):
        self.state = 'done'

    @api.multi
    def action_invoice_create(self, grouped=False, final=False):
        '''
        @param self: object pointer
        '''
        emp_lst = []
        invoice_id = (self.order_id.action_invoice_create(grouped=False,
                                                          final=False))
        for line in self:
            values = {'invoiced': True,
                      'spa_invoice_id': invoice_id
                      }
            line.write(values)
            for rec in line.service_lines:
                emp_lst.append(rec.employee_id)
            for emp in emp_lst:
                emp_obj = self.env['spa.employee'
                                    ].search([('name', '=', emp.name)])
                emp_obj.write({'isemployee': True})
        return invoice_id



    @api.multi
    def action_cancel(self):
        '''
        @param self: object pointer
        '''
        if not self.order_id:
            raise ValidationError(_('Order id is not available'))
        for sale in self:
            for invoice in sale.invoice_ids:
                invoice.state = 'cancel'
        return self.order_id.action_cancel()

    # @api.multi
    # def action_confirm(self):
    #     for order in self.order_id:
    #         order.state = 'sale'
    #         order.order_line._action_procurement_create()
    #         if not order.project_id:
    #             for line in order.order_line:
    #                 if line.product_id.invoice_policy == 'cost':
    #                     order._create_analytic_account()
    #                     break
    #     if self.env['ir.values'].get_default('sale.config.settings',
    #                                          'auto_done_setting'):
    #         self.order_id.action_done()

    @api.multi
    def _action_confirm(self):
        """ Implementation of additionnal mecanism of Sales Order confirmation.
            This method should be extended when the confirmation should generated
            other documents. In this method, the SO are in 'sale' state (not yet 'done').
        """
        if self.env.context.get('send_email'):
            self.force_quotation_send()

        # create an analytic account if at least an expense product
        if any([expense_policy != 'no' for expense_policy in self.order_line.mapped('product_id.expense_policy')]):
            if not self.analytic_account_id:
                self._create_analytic_account()

        return True

    @api.multi
    def action_confirm(self):
        if self._get_forbidden_state_confirm() & set(self.mapped('state')):
            raise UserError(_(
                'It is not allowed to confirm an order in the following states: %s'
            ) % (', '.join(self._get_forbidden_state_confirm())))

        for order in self.filtered(lambda order: order.partner_id not in order.message_partner_ids):
            order.message_subscribe([order.partner_id.id])
        self.write({
            'state': 'sale',
            'confirmation_date': fields.Datetime.now()
        })
        self._action_confirm()
        if self.env['ir.config_parameter'].sudo().get_param('sale.auto_done_setting'):
            self.action_done()
        return True

    def _get_forbidden_state_confirm(self):
        return {'done', 'cancel'}

    @api.multi
    def action_cancel_draft(self):
        '''
        @param self: object pointer
        '''
        if not len(self._ids):
            return False
        query = "select id from sale_order_line \
        where order_id IN %s and state=%s"
        self._cr.execute(query, (tuple(self._ids), 'cancel'))
        cr1 = self._cr
        line_ids = map(lambda x: x[0], cr1.fetchall())
        self.write({'state': 'draft', 'invoice_ids': []})
        sale_line_obj = self.env['sale.order.line'].browse(line_ids)
        sale_line_obj.write({'invoiced': False, 'state': 'draft',
                             'invoice_lines': [(6, 0, [])]})
        return True

    @api.multi
    def spa_invoice_create(self):
        inv_obj = self.env['account.invoice']
        inv_line_obj = self.env['account.invoice.line']
        if self.partner_id:
            supplier = self.partner_id
        company_id = self.env['res.users'].browse(1).company_id
        currency_salon = company_id.currency_id.id

        inv_data = {
            'name': supplier.name,
            'reference': supplier.name,
            'account_id': supplier.property_account_payable_id.id,
            'partner_id': supplier.id,
            'currency_id': currency_salon,
            'journal_id': 1,
            'origin': self.name,
            'company_id': company_id.id,
        }
        inv_id = inv_obj.with_context({'folio_id':self.id}).create(inv_data)
        self.spa_invoice_id = inv_id.id
        # product_id = self.env['spa.services'].search([("name", "=", "Salon Service")])
        for records in self.service_lines:
            if records.product_id:
                product_id = records.product_id
                if product_id.property_account_income_id.id:
                    income_account = product_id.property_account_income_id.id
                elif product_id.categ_id.property_account_income_categ_id.id:
                    income_account = product_id.categ_id.property_account_income_categ_id.id
                else:
                    raise UserError(_('Please define income account for this product: "%s" (id:%d).') % (product_id.name,
                                                                                                         product_id.id))
                inv_line_data = {
                    'name': records.folio_id.name,
                    'account_id': income_account,
                    'price_unit': records.price_unit,
                    'quantity': 1,
                    'product_id': product_id.id,
                    'invoice_id': inv_id.id,
                }
                inv_line_obj.create(inv_line_data)

        imd = self.env['ir.model.data']
        action = imd.xmlid_to_object('account.action_invoice_tree1')
        list_view_id = imd.xmlid_to_res_id('account.invoice_tree')
        form_view_id = imd.xmlid_to_res_id('account.invoice_form')

        result = {
            'name': action.name,
            'help': action.help,
            'type': 'ir.actions.act_window',
            'views': [[list_view_id, 'tree'], [form_view_id, 'form'], [False, 'graph'], [False, 'kanban'],
                      [False, 'calendar'], [False, 'pivot']],
            'target': action.target,
            'context': action.context,
            'res_model': 'account.invoice',
        }
        if len(inv_id) > 1:
            result['domain'] = "[('id','in',%s)]" % inv_id.ids
        elif len(inv_id) == 1:
            result['views'] = [(form_view_id, 'form')]
            result['res_id'] = inv_id.ids[0]
        else:
            result = {'type': 'ir.actions.act_window_close'}

        return result


    @api.multi
    def spa_invoice_print(self):
        if self.spa_invoice_id:
            return self.spa_invoice_id.invoice_print()
        else:
            raise ValidationError(
                                 _('Invoice not created yet.'))


class SpaServiceLine(models.Model):

    @api.multi
    def copy(self, default=None):
        '''
        @param self: object pointer
        @param default: dict of default values to be set
        '''
        return super(SpaServiceLine, self).copy(default=default)

    @api.model
    def _service_start_time(self):
        if 'start' in self._context:
            return self._context['start']
        return time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

    @api.model
    def _service_end_time(self):
        if 'end' in self._context:
            return self._context['end']
        return time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

    _name = 'spa.service.line'
    _description = 'Spa Service line'

    service_line_id = fields.Many2one('sale.order.line', 'Service Line',
                                      required=True, delegate=True,
                                      ondelete='cascade')
    folio_id = fields.Many2one('spa.folio', 'Folio', ondelete='cascade')
    ser_start_time = fields.Datetime('Start time',
                                     default=_service_start_time)
    ser_end_time = fields.Datetime('End time',
                                   default=_service_end_time)
    employee_id = fields.Many2one('spa.employee', 'Employee', ondelete='cascade')

    status = fields.Selection(string='state', related='folio_id.state')
    is_reserved = fields.Boolean('Is Reserved',
                                 help='True when folio line created from \
                                     Reservation')
    booking_flag = fields.Boolean(related='folio_id.booking_flag')

    @api.model
    def create(self, vals):
        """
        Overrides orm create method.
        @param self: The object pointer
        @param vals: dictionary of fields value.
        @return: new record set for hotel service line.
        """
        if 'folio_id' in vals:
            folio = self.env['spa.folio'].browse(vals['folio_id'])
            vals.update({'order_id': folio.order_id.id})
        return super(SpaServiceLine, self).create(vals)

    @api.multi
    def unlink(self):
        """
        Overrides orm unlink method.
        @param self: The object pointer
        @return: True/False.
        """
        s_line_obj = self.env['sale.order.line']
        for line in self:
            if line.service_line_id:
                sale_unlink_obj = s_line_obj.browse([line.service_line_id.id])
                sale_unlink_obj.unlink()
        return super(SpaServiceLine, self).unlink()

    @api.onchange('product_id')
    def product_id_change(self):
        '''
        @param self: object pointer
        '''
        if self.product_id and self.folio_id.partner_id:
            self.name = self.product_id.name
            self.price_unit = self.product_id.list_price
            self.product_uom = self.product_id.uom_id
            tax_obj = self.env['account.tax']
            prod = self.product_id
            self.price_unit = tax_obj._fix_tax_included_price(prod.price,
                                                              prod.taxes_id,
                                                              self.tax_id)

    @api.onchange('product_uom')
    def product_uom_change(self):
        '''
        @param self: object pointer
        '''
        if not self.product_uom:
            self.price_unit = 0.0
            return
        self.price_unit = self.product_id.list_price
        if self.folio_id.partner_id:
            prod = self.product_id.with_context(
                lang=self.folio_id.partner_id.lang,
                partner=self.folio_id.partner_id.id,
                quantity=1,
                date_order=False,
                pricelist=self.folio_id.pricelist_id.id,
                uom=self.product_uom.id
            )
            tax_obj = self.env['account.tax']
            self.price_unit = tax_obj._fix_tax_included_price(prod.price,
                                                              prod.taxes_id,
                                                              self.tax_id)



    @api.onchange('ser_start_time', 'ser_end_time')
    def on_change_time(self):
        '''
        When you change checkin_date or checkout_date it will checked it
        and update the qty of spa service line
        -----------------------------------------------------------------
        @param self: object pointer
        '''
        if not self.ser_start_time:
            time_a = time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            self.ser_start_time = time_a
        if not self.ser_end_time:
            self.ser_end_time = time_a
        if self.ser_end_time < self.ser_start_time:
            raise UserError(_('End time must be greater or equal start time'))

    @api.multi
    def button_confirm(self):
        '''
        @param self: object pointer
        '''
        for folio in self:
            ser_line = folio.service_line_id
            line = ser_line.button_confirm()
        return line

    @api.multi
    def button_done(self):
        '''
        @param self: object pointer
        '''
        for folio in self:
            ser_line = folio.service_line_id
            line = ser_line.button_done()
        return line

    @api.multi
    def copy_data(self, default=None):
        '''
        @param self: object pointer
        @param default: dict of default values to be set
        '''
        sale_line_obj = self.env['sale.order.line'
                                 ].browse(self.service_line_id.id)
        return sale_line_obj.copy_data(default=default)


class SpaServiceType(models.Model):

    _name = "spa.service.type"
    _description = "Service Type"

    name = fields.Char('Service Name', size=64, required=True)
    isservicetype = fields.Boolean('Is Service Type')
    service_id = fields.Many2one('spa.service.type', string='Service Category')
    child_id = fields.One2many('spa.service.type', 'service_id',
                               'Child Categories')
    image = fields.Binary(
        "Image",
        help="Image of the service type")


    @api.multi
    def name_get(self):
        def get_names(cat):
            """ Return the list [cat.name, cat.service_id.name, ...] """
            res = []
            while cat:
                res.append(cat.name)
                cat = cat.service_id
            return res
        return [(cat.id, " / ".join(reversed(get_names(cat)))) for cat in self]

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if not args:
            args = []
        if name:
            # Be sure name_search is symetric to name_get
            category_names = name.split(' / ')
            parents = list(category_names)
            child = parents.pop()
            domain = [('name', operator, child)]
            if parents:
                names_ids = self.name_search(' / '.join(parents), args=args,
                                             operator='ilike', limit=limit)
                category_ids = [name_id[0] for name_id in names_ids]
                if operator in expression.NEGATIVE_TERM_OPERATORS:
                    categories = self.search([('id', 'not in', category_ids)])
                    domain = expression.OR([[('service_id', 'in',
                                              categories.ids)], domain])
                else:
                    domain = expression.AND([[('service_id', 'in',
                                               category_ids)], domain])
                for i in range(1, len(category_names)):
                    domain = [[('name', operator,
                                ' / '.join(category_names[-1 - i:]))], domain]
                    if operator in expression.NEGATIVE_TERM_OPERATORS:
                        domain = expression.AND(domain)
                    else:
                        domain = expression.OR(domain)
            categories = self.search(expression.AND([domain, args]),
                                     limit=limit)
        else:
            categories = self.search(args, limit=limit)
        return categories.name_get()


class SpaServices(models.Model):

    _name = 'spa.services'
    _description = 'Spa Services and its charges'

    service_id = fields.Many2one('product.product', 'Service_id',
                                 required=True, ondelete='cascade',
                                 delegate=True)
    categ_id = fields.Many2one('spa.service.type', string='Service Category',
                               required=True)
    type = fields.Selection([
        ('consu', _('Consumable')),
        ('service', _('Service'))], string='Product Type', default='service', required=True,
        help='A stockable product is a product for which you manage stock. The "Inventory" app has to be installed.\n'
             'A consumable product, on the other hand, is a product for which stock is not managed.\n'
             'A service is a non-material product you provide.\n'
             'A digital content is a non-material product you sell online. The files attached to the products are the one that are sold on '
             'the e-commerce such as e-books, music, pictures,... The "Digital Product" module has to be installed.')

    hours = fields.Integer(string="Hrs")
    mins = fields.Integer(string="Mins")
    service_image = fields.Binary(string='Service Image')

class ResCompany(models.Model):

    _inherit = 'res.company'

    additional_hours = fields.Integer('Additional Hours',
                                      help="Provide the min hours value for \
                                      check in, checkout days, whatever the \
                                      hours will be provided here based \
                                      on that extra days will be calculated.")
    spa_buffer_time = fields.Float('Buffer Time',
                                    help="This buffer time will be applicable \
                                    to the employee's to take next customer, \
                                    when you reserve for the services.")





class AccountInvoice(models.Model):

    _inherit = 'account.invoice'

    @api.model
    def create(self, vals):
        res = super(AccountInvoice, self).create(vals)
        if self._context.get('folio_id'):
            folio = self.env['spa.folio'].browse(self._context['folio_id'])
            folio.write({'spa_invoice_id': res.id,
                         'invoice_status': 'invoiced'})
        return res
