# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from dateutil.relativedelta import relativedelta
from dateutil import parser
from odoo import api, fields, models


class FolioReport(models.AbstractModel):
    _name = 'report.spa.report_spa_folio'

    def get_data(self, date_start, date_end):
        folio_obj = self.env['spa.service.line']
        res = folio_obj.search([('ser_start_time', '>=', date_start),
                                 ('ser_end_time', '<=', date_end)])
        return res

    @api.model
    def _get_report_values(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_ids',
                                                                []))
        date_start = data['form'].get('date_start', fields.Date.today())
        date_end = data['form'].get('date_end', str(datetime.now() +
                                    relativedelta(months=+1,
                                                  day=1, days=-1))[:10])
        rm_act = self.with_context(data['form'].get('used_context', {}))
        data_res = rm_act.get_data(date_start, date_end)
        docargs = {
            'doc_ids': docids,
            'doc_model': 'spa.service.line',
            'data': data['form'],
            'docs': docs,
            'time': time,
            'folio_data': data_res,
        }
        docargs['data'].update({'date_end':
                                parser.parse(docargs.get('data').
                                             get('date_end')).
                                strftime('%m/%d/%Y')})
        docargs['data'].update({'date_start':
                                parser.parse(docargs.get('data').
                                             get('date_start')).
                                strftime('%m/%d/%Y')})
        render_model = 'spa.report_spa_folio'
        return docargs