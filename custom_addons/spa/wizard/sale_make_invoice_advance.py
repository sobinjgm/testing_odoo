# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

    @api.model
    def _get_advance_payment(self):
        ctx = self._context.copy()
        if self._context.get('active_model') == 'spa.reservation.line':
            spa_res_line = self.env['spa.reservation.line']
            res_line = spa_res_line.browse(self._context.get('active_ids',
                                                       []))
            spa = res_line.reservation_id.folio_id

            ctx.update({'active_ids': [spa.order_id.id],
                        'active_id': spa.order_id.id})
        return super(SaleAdvancePaymentInv,
                     self.with_context(ctx))._get_advance_payment_method()
    advance_payment_method = fields.Selection([('delivered',
                                                'Invoiceable lines'),
                                               ('all',
                                                'Invoiceable lines\
                                                (deduct down payments)'),
                                               ('percentage',
                                                'Down payment (percentage)'),
                                               ('fixed',
                                                'Down payment (fixed\
                                                amount)')],
                                              string='What do you want\
                                              to invoice?',
                                              default=_get_advance_payment,
                                              required=True)

    @api.multi
    def create_invoices(self):
        ctx = self._context.copy()
        if self._context.get('active_model') == 'spa.reservation.line':
            spa_res_line = self.env['spa.reservation.line']
            res_line = spa_res_line.browse(self._context.get('active_ids',
                                                             []))
            spa = res_line.reservation_id.folio_id
            ctx.update({'active_ids': [spa.order_id.id],
                        'active_id': spa.order_id.id,
                        'folio_id': spa.id})
        res = super(SaleAdvancePaymentInv,
                    self.with_context(ctx)).create_invoices()
        return res
