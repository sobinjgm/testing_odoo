{
    'name' : 'Booking Calendar',
    'version': '1.0',
    'summary': 'Customer booking calendar view',
    'category': 'Tools',
    'description':
        """        """,
    'data': [
        "views/booking_calendar.xml",
    ],
    'depends' : ['base','web','spa_reservation', 'hr_holidays'],
    'qweb': ['static/src/xml/calendar.xml'],
    'application': True,
}
