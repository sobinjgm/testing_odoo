from odoo import api, fields, models
import time
from datetime import datetime as dt
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as server_dt
import pytz


class spa_reservation_line(models.Model):
    _inherit = "spa.reservation.line"

    '''This function returns the booking data
       which will be opt for the Calender Day view '''
    @api.model
    def get_calendar_data(self, employee):
        result = {}
        employees = []
        resources = []
        events = []
        spa_emp_obj = self.env['spa.employee']
        current_date = dt.now().strftime("%Y-%m-%d %H:%M:%S")
        current_date = dt.strptime(current_date, "%Y-%m-%d %H:%M:%S")
        current_date_tz = dt.strptime(str(current_date), server_dt).replace(tzinfo=pytz.UTC).astimezone(
            pytz.timezone(self.env.user.tz or 'UTC'))
        result['date'] = str(current_date_tz)[:10]

        # employee details
        emp_domain = [('active', '=', True)]
        booking_domain = [('state', '=', 'order_confirm')]
        leave_domain = [('state', '=', 'validate'),('employee_id.isemployee','=',True)]
        emp_ids = spa_emp_obj.search(emp_domain, order='name')
        for emp in emp_ids:
            employees.append({'id': emp.id, 'title': emp.name})
        if employee and employee != 'all':
            emp_domain.append(('id', '=', int(employee)))
            booking_domain.append(('employee.id', '=', int(employee)))
            emp_id = spa_emp_obj.search([('id','=',int(employee))])
            leave_domain.append(('employee_id', '=', emp_id.employee_id.id))
        emp_ids = spa_emp_obj.search(emp_domain, order='name')
        for emp in emp_ids:
            busHrs_list = []
            if emp.employee_shift_timing_ids:
                for shift in emp.employee_shift_timing_ids:
                    busHrs_dict = {}
                    working_day = int(shift.name)+1
                    busHrs_dict['dow'] = [0 if working_day==7 else working_day]
                    busHrs_dict['start'] = shift.hour_from
                    busHrs_dict['end'] = shift.hour_to
                    busHrs_list.append(busHrs_dict)
            else:
                busHrs_list = [{'dow':[], 'start':'00:00', 'end':'23:55'}]
            resources.append({'id': emp.id, 'title': emp.name, 'businessHours': busHrs_list})
            
        # booking details
        count = 0
        eventColor = ['','green','orange','brown','blue','yellow']
        booking_details = self.search(booking_domain)
        for det in booking_details:
            res_dict = {}
            # start time
            if det.start_time:
                if count == 6:
                    count = 0
                res_dict['id'] = det.id
                res_dict['resourceId'] = det.employee.id
                res_dict['title'] = str(det.reservation_id.reservation_no) + ' - ' + str(det.employee.name)
                res_dict['resources'] = [det.employee.id]
                res_dict['allDay'] = False
                res_dict['leave'] = False
                res_dict['backgroundColor'] = eventColor[count]
                start_time = dt.strptime(str(det.start_time), "%Y-%m-%d %H:%M:%S")
                start_time_tz = dt.strptime(str(start_time), server_dt).replace(tzinfo=pytz.UTC).astimezone(
                    pytz.timezone(self.env.user.tz or 'UTC'))
                res_dict['start'] = str(start_time_tz)[:19]
                # end time
                if det.end_time:
                    end_time = dt.strptime(str(det.end_time), "%Y-%m-%d %H:%M:%S")
                    end_time_tz = dt.strptime(str(end_time), server_dt).replace(tzinfo=pytz.UTC).astimezone(
                        pytz.timezone(self.env.user.tz or 'UTC'))
                    res_dict['end'] = str(end_time_tz)[:19]
                events.append(res_dict)
                count += 1
        # leave details
        leave_details = self.env['hr.leave'].search(leave_domain)
        for det in leave_details:
            res_dict = {}
            emp_id = spa_emp_obj.search([('employee_id','=',det.employee_id.id)])[0]
            # start time
            if det.date_from and emp_id:
                res_dict['id'] = det.id
                res_dict['resourceId'] = emp_id.id
                res_dict['title'] = str(det.holiday_status_id.name) +' - '+ str(det.name)
                res_dict['resources'] = [emp_id.id]
                res_dict['allDay'] = False
                res_dict['leave'] = True
                res_dict['backgroundColor'] = 'red'
                start_time = dt.strptime(str(det.date_from), "%Y-%m-%d %H:%M:%S")
                start_time_tz = dt.strptime(str(start_time), server_dt).replace(tzinfo=pytz.UTC).astimezone(
                    pytz.timezone(self.env.user.tz or 'UTC'))
                res_dict['start'] = str(start_time_tz)[:19]
                # end time
                if det.date_to:
                    end_time = dt.strptime(str(det.date_to), "%Y-%m-%d %H:%M:%S")
                    end_time_tz = dt.strptime(str(end_time), server_dt).replace(tzinfo=pytz.UTC).astimezone(
                        pytz.timezone(self.env.user.tz or 'UTC'))
                    res_dict['end'] = str(end_time_tz)[:19]
                events.append(res_dict)

        result['employees'] = employees
        result['resources'] = resources
        result['events'] = events
        return result
