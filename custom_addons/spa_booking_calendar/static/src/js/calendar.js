odoo.define('spa_booking_calendar.calendar', function (require) {
"use strict";

var AbstractAction = require('web.AbstractAction');
var ControlPanelMixin = require('web.ControlPanelMixin');
var core = require('web.core');
var data = require('web.data');
var Widget = require('web.Widget');
//var Form_Common = require('web.form_common');
var rpc = require('web.rpc');
var framework = require('web.framework');
var ActionManager = require('web.ActionManager');
var time = require('web.time');


var _t = core._t;
var _lt = core._lt;
var QWeb = core.qweb;

var BookingSummaryCalendar = AbstractAction.extend(ControlPanelMixin, {
    
    events: {
        'click .fc-button-agendaDay': 'get_calendar_data',
        'change #employee_selector': 'get_calendar_data',
    },
    start: function() {
        var self=this;
        self.$el.empty();
        self.get_calendar_data();
    },
    get_calendar_data: function(event){
        var self=this;
        var employee = $('#employee_selector').val();
        self.$el.empty();
        if(!employee)
            employee = 'all';
        rpc.query({
                model: 'spa.reservation.line',
                method: 'get_calendar_data',
                args: [employee],
            }).then(function(result){
            if(result){
                self.$el.append(QWeb.render('calendar.header.template', {employees: result['employees']}));
                $('#employee_selector').val(employee);
                self.$el.append(QWeb.render('calendar.template', {employees: result['employees']}));
                self.$el.find('#calendar').fullCalendar({
                    defaultView: 'agendaDay',
                    defaultDate: result['date'],
                    editable: false,
                    selectable: true,
                    eventLimit: true, // allow "more" link when too many events
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'agendaDay,agendaWeek,month,listWeek'
                    },
                    resources: result['resources'],
                    events: result['events'],
                    selectConstraint: "businessHours",
                    select: function(start, end, jsEvent, view, resource) {
                        if(view.name=='agendaDay'){
                            self.do_action({
                                type: 'ir.actions.act_window',
                                res_model: "spa.reservation.line",
                                views: [[false, 'form']],
                                target: 'new',
                                context: {'reservation_start':start.format()},
                            });
                        }
                    },
                    dayClick: function(date, jsEvent, view, resource) {
                        console.log('dayClick',date.format(),resource ? resource.id : '(no resource)');
                        
                    },
                    eventClick: function(calEvent, jsEvent, view) {
                        if(!calEvent.leave){
                            self.do_action({
                                    name: 'Reservation Details',
                                    type: 'ir.actions.act_window',
                                    res_model: "spa.reservation.line",
                                    res_id: calEvent.id,
                                    views: [[false, 'form']],
                                    target: 'current',
                                    context: {},
                                });
                        }
                    },
                    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                });
                $("#calendar").on("swipe",function(){
                    $(this).hide();
                });
            }
        });
    },
    
});

core.action_registry.add('booking.calendar', BookingSummaryCalendar);
});
