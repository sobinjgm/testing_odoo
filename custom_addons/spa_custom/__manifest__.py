# -*- coding: utf-8 -*-
{
    'name': "Spa Custom",

    'summary': """
       Spa Customization""",

    'description': """
          Spa Customization
    """,

    'author': "Infintor",
    'website': "http://www.infintor.com",
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','account','spa_reservation'],

    # always loaded
    'data': [
        
        'views.xml',
    ],
   
    'installable': True,
    'application': True,
    'auto_install': False,
}
