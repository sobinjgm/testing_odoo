# -*- coding: utf-8 -*-
from odoo import models, fields, api
import logging

_logger = logging.getLogger(__name__)


from num2words import num2words
STATES = [
    ('walkin', 'Walkin'),
    ('home', 'Home'),

]
MODES = [
    ('walkin', 'Walkin'),
    ('home', 'Home'),
    ('both', 'Both'),
]
        
class SpaServices(models.Model):
    _inherit = 'spa.services'
    spa_service_type = fields.Selection(STATES)
	
	
class EmpworkType(models.Model):
    _inherit = 'spa.employee'
    service_type = fields.Selection(MODES)
     	
