# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Spa Reservation Management',
    'version': '10.0.1.0.0',
    'author': 'Infintor',
    'category': 'Generic Modules/Spa Reservation',
    'license': 'AGPL-3',
    'depends': ['spa', 'stock', 'mail', 'project'],
    'data': [
"security/ir.model.access.csv",
        "views/service_res.xml",
        "wizard/spa_reservation_wizard.xml",
        "report/spa_reservation_report.xml",
        "views/spa_reservation_sequence.xml",
        "views/spa_reservation_view.xml",
        "views/email_temp_view.xml",
        "data/spa_scheduler.xml",
    ],
    'css': ['static/src/css/employee_summary.css'],
    'installable': True,
    'auto_install': False,
}
