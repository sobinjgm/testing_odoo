# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as dt
import pytz
import string

class ResUsers(models.Model):

    _inherit = 'res.users'

    def get_spa_service_categories(self):
        try:
            categories= self.env['spa.service.type'].search([])
            categ_list=[]
            for category in categories:
                vals={
                    'id': category.id,
                    'name':category.name,
                    'image':category.image
                }
                categ_list.append(vals)
            return {
                    'status':200,
                    'description':'Success',
                    'categories':categ_list
                }
        except:
            return {
                'status':404,
                'description':'Fail'
            }


    def get_spa_services(self,category_id):
        try:
            if category_id:
                services = self.env['spa.services'].search([('categ_id','=',int(category_id))])
                service_list = []
                for service in services:
                    vals = {
                        'id': service.id,
                        'name': service.name,
                        'price':service.list_price,
                        'image':service.image_medium,
                        'category_id': service.categ_id and service.categ_id.id or False,
                        'category_name': service.categ_id and service.categ_id.name or False,
                        'description': service.description,
                        'service_hours': service.hours,
                        'service_mins': service.mins,
                        'service_image':service.service_image or ''
                    }
                    service_list.append(vals)
                return {
                    'status':200,
                    'description':'Success',
                    'services':service_list

                }
            else:
                return {
                    'status':404,
                    'description':'Fail'
                }
        except:
            return {
                'status':404,
                'description':'Fail'
            }

    def get_spa_service_details(self, service_id):
        try:
            if service_id:
                services = self.env['spa.services'].search([('id', '=', int(service_id))])
                service_list = []
                for service in services:
                    vals = {
                        'id': service.id,
                        'name': service.name,
                        'price': service.list_price,
                        'image': service.image_medium,
                        'category_id':service.categ_id and service.categ_id.id or False,
                        'category_name': service.categ_id and service.categ_id.name or False,
                        'description':service.description,
                        'service_hours':service.hours,
                        'service_mins':service.mins,
                        'service_image':service.service_image or ''

                    }
                    service_list.append(vals)
                return {
                    'status': 200,
                    'description': 'Success',
                    'service_details': service_list

                }
            else:
                return {
                    'status': 404,
                    'description': 'Fail'
                }
        except:
            return {
                'status': 404,
                'description': 'Fail'
            }

    def create_spa_user(self, name,login,password,mobile):
        try:
            user_exists = self.search([('login','=',login)])
            if user_exists:
                return {
                    'status': 400,
                    'description': 'User exists with same login',
                }
            else:
                user_id = self.create({'name':name,'email':login,'login':login,'password':password,'mobile':mobile,'groups_id':[(6,0,[self.env.ref('spa.group_spa_user').id,self.env.ref('hr.group_hr_user').id,self.env.ref('sales_team.group_sale_manager').id])]})
                return {
                    'status':200,
                    'user_id':user_id[0].id,
                    'description':'user created succesfully'
                }
        except:
            return {
                'status': 404,
                'description': 'Fail'
            }


    def get_beautician_list(self, service_id,reservation_date,today_reservation):
        #try:
            service_obj=self.env['spa.services'].browse(int(service_id))
            allowed_employees = self.env['spa.employee'].search([('allowed_services', '=', int(service_id))])
            beautician_list=[]
            for employee in allowed_employees:
                beautician_dict = {}
                user_tz = self.search([('id','=',2)])[0].tz
                if user_tz:
                    s = datetime.now(pytz.timezone(user_tz)).strftime('%z')
                    sign=s[0]
                    hour=s[1:3]
                    minute = s[3:]
                else:
                    hour= 3
                    minute =0
                    sign='+'
                weekdays= ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
                reservation_date1 = reservation_date + ' ' + '00:00:00'
                compare_date = datetime.strptime(reservation_date1, '%Y-%m-%d %H:%M:%S')
                current_day = compare_date.strftime('%A')
                index_current_day = weekdays.index(current_day)
                employee_working_day = self.env['spa.employee.shift.timing'].search([('employee_id','=',employee.id),('name','=',index_current_day)])

                service_duration_hours = service_obj.hours
                service_duration_mins = service_obj.mins
                duration_service = float(str(service_obj.hours) + '.' + str(service_obj.mins))

                if employee_working_day:
                    from_time = employee_working_day[0].hour_from
                    from_hour_min = from_time.split(':')
                    to_time = employee_working_day[0].hour_to
                    to_hour_min = to_time.split(':')
                    time_slots=[]
                    initial_calc_from_time = compare_date + timedelta(hours=int(from_hour_min[0]), minutes=int(from_hour_min[1]))
                    calc_from_time = compare_date + timedelta(hours=int(from_hour_min[0]), minutes=int(from_hour_min[1]))
                    if today_reservation:
                        today_reservation_utc = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

                        reservation_today_date = datetime.strptime(today_reservation_utc, '%Y-%m-%d %H:%M:%S')
                        if sign == '+':
                            current_reserved_from_time = reservation_today_date + timedelta(hours=int(hour), minutes=int(minute))
                        else:
                            current_reserved_from_time = reservation_today_date - timedelta(hours=int(hour), minutes=int(minute))
                        if calc_from_time < current_reserved_from_time:
                            current_reserved_from_time = current_reserved_from_time + timedelta(hours=1,minutes=30)
                            current_reserved_from_time = current_reserved_from_time.replace(minute=0, second=0)
                            initial_calc_from_time = current_reserved_from_time
                            calc_from_time = current_reserved_from_time
                        else:
                            calc_from_time = calc_from_time + timedelta(hours=1,minutes=30)
                            calc_from_time = calc_from_time.replace(minute=0, second=0)
                    calc_to_time = compare_date + timedelta(hours=int(to_hour_min[0]), minutes=int(to_hour_min[1]))
                    while(calc_from_time <= calc_to_time):

                        serv_to_time = calc_from_time + timedelta(hours=int(service_duration_hours),
                                                                  minutes=int(service_duration_mins))
                        if (serv_to_time <= calc_to_time):
                            if sign == '+':
                                reserved_from_time = calc_from_time - timedelta(hours=int(hour), minutes=int(minute))
                            else:
                                reserved_from_time = calc_from_time + timedelta(hours=int(hour), minutes=int(minute))
                            reserved_to_time = reserved_from_time + timedelta(hours=int(service_duration_hours),
                                                                  minutes=int(service_duration_mins))

                            reserved_ids = self.env['spa.reservation.line'].search(
                                [('state', '=', 'order_confirm'), ('employee', '=', employee.id), '|', '&',
                                 ('start_time', '<=', str(reserved_from_time)), ('end_time', '>', str(reserved_from_time)), '&',
                                 ('start_time', '>=', str(reserved_from_time)), ('start_time', '<=', str(reserved_to_time))])

                            if not reserved_ids:
                                time_slots.append(calc_from_time)
                                calc_from_time = calc_from_time + timedelta(minutes=60)
                            else:
                                if initial_calc_from_time:
                                    if initial_calc_from_time != calc_from_time:

                                        calc_from_time = calc_from_time - timedelta(minutes=60)
                                    else:
                                        calc_from_time = calc_from_time
                                    while(calc_from_time <= calc_to_time):
                                        calc_from_time = calc_from_time + timedelta(minutes=15)

                                        serv_to_time = calc_from_time + timedelta(hours=int(service_duration_hours),
                                                                                  minutes=int(service_duration_mins))
                                        if (serv_to_time <= calc_to_time):
                                            if sign == '+':
                                                reserved_from_time = calc_from_time - timedelta(hours=int(hour),
                                                                                                minutes=int(minute))
                                            else:
                                                reserved_from_time = calc_from_time + timedelta(hours=int(hour),
                                                                                                minutes=int(minute))
                                            reserved_to_time = reserved_from_time + timedelta(hours=int(service_duration_hours),
                                                                                              minutes=int(
                                                                                                  service_duration_mins))

                                            reserved_ids = self.env['spa.reservation.line'].search(
                                                [('state', '=', 'order_confirm'), ('employee', '=', employee.id), '|', '&',
                                                 ('start_time', '<=', str(reserved_from_time)),
                                                 ('end_time', '>', str(reserved_from_time)), '&',
                                                 ('start_time', '>=', str(reserved_from_time)),
                                                 ('start_time', '<=', str(reserved_to_time))])
                                            if not reserved_ids:
                                                time_slots.append(calc_from_time)
                                                calc_from_time = calc_from_time + timedelta(minutes=60)
                                                break
                                        else:
                                            break;
                        else:
                            break;

                    if time_slots:
                        slots=[]
                        for j in time_slots:
                            p =str(j)
                            slots.append(p[11:16])
                        beautician_dict ={'employee':employee.name,'employee_id':employee.id,'slots':slots}
                        beautician_list.append(beautician_dict)

            return {
                'status': 200,
                'description': 'success',
                'beautician': beautician_list
            }

       # except:
        #    return {
         #       'status': 404,
          #      'description': 'Fail'
           # }

    def get_beautician_slots(self, service_id, reservation_date,employee_id,today_reservation):
    #    try:
            service_obj = self.env['spa.services'].browse(int(service_id))

            beautician_list = []
            beautician_dict = {}
            employee_obj= self.env['spa.employee'].search([('id','=',int(employee_id))])
            if employee_obj:
                employee=employee_obj[0]
                user_tz = self.search([('id', '=', 2)])[0].tz
                if user_tz:
                    s = datetime.now(pytz.timezone(user_tz)).strftime('%z')
                    sign = s[0]
                    hour = s[1:3]
                    minute = s[3:]
                else:
                    hour = 3
                    minute = 0
                    sign = '+'
                weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
                reservation_date1 = reservation_date + ' ' + '00:00:00'
                compare_date = datetime.strptime(reservation_date1, '%Y-%m-%d %H:%M:%S')
                current_day = compare_date.strftime('%A')
                index_current_day = weekdays.index(current_day)
                employee_working_day = self.env['spa.employee.shift.timing'].search(
                    [('employee_id', '=', employee.id), ('name', '=', index_current_day)])

                service_duration_hours = service_obj.hours
                service_duration_mins = service_obj.mins
                duration_service = float(str(service_obj.hours) + '.' + str(service_obj.mins))

                if employee_working_day:
                    from_time = employee_working_day[0].hour_from
                    from_hour_min = from_time.split(':')
                    to_time = employee_working_day[0].hour_to
                    to_hour_min = to_time.split(':')
                    time_slots = []
                    initial_calc_from_time = compare_date + timedelta(hours=int(from_hour_min[0]),
                                                                      minutes=int(from_hour_min[1]))
                    calc_from_time = compare_date + timedelta(hours=int(from_hour_min[0]), minutes=int(from_hour_min[1]))
                    if today_reservation:
                        today_reservation_utc = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
                        reservation_today_date = datetime.strptime(today_reservation_utc, '%Y-%m-%d %H:%M:%S')
                        if sign == '+':
                            current_reserved_from_time = reservation_today_date + timedelta(hours=int(hour), minutes=int(minute))
                        else:
                            current_reserved_from_time = reservation_today_date - timedelta(hours=int(hour), minutes=int(minute))
                        if calc_from_time < current_reserved_from_time:
                            current_reserved_from_time = current_reserved_from_time + timedelta(hours=1, minutes=30)
                            current_reserved_from_time = current_reserved_from_time.replace(minute=0, second=0)
                            initial_calc_from_time = current_reserved_from_time
                            calc_from_time = current_reserved_from_time
                        else:
                            calc_from_time = calc_from_time + timedelta(hours=1, minutes=30)
                            calc_from_time = calc_from_time.replace(minute=0, second=0)

                    calc_to_time = compare_date + timedelta(hours=int(to_hour_min[0]), minutes=int(to_hour_min[1]))
                    while (calc_from_time <= calc_to_time):

                        serv_to_time = calc_from_time + timedelta(hours=int(service_duration_hours),
                                                                  minutes=int(service_duration_mins))
                        if (serv_to_time <= calc_to_time):
                            if sign == '+':
                                reserved_from_time = calc_from_time - timedelta(hours=int(hour), minutes=int(minute))
                            else:
                                reserved_from_time = calc_from_time + timedelta(hours=int(hour), minutes=int(minute))
                            reserved_to_time = reserved_from_time + timedelta(hours=int(service_duration_hours),
                                                                              minutes=int(service_duration_mins))

                            reserved_ids = self.env['spa.reservation.line'].search(
                                [('state', '=', 'order_confirm'), ('employee', '=', employee.id), '|', '&',
                                 ('start_time', '<=', str(reserved_from_time)), ('end_time', '>', str(reserved_from_time)), '&',
                                 ('start_time', '>=', str(reserved_from_time)), ('start_time', '<=', str(reserved_to_time))])

                            if not reserved_ids:
                                time_slots.append(calc_from_time)
                                calc_from_time = calc_from_time + timedelta(minutes=60)
                            else:
                                if initial_calc_from_time:
                                    if initial_calc_from_time != calc_from_time:

                                        calc_from_time = calc_from_time - timedelta(minutes=60)
                                    else:
                                        calc_from_time = calc_from_time

                                    while (calc_from_time <= calc_to_time):
                                        calc_from_time = calc_from_time + timedelta(minutes=15)

                                        serv_to_time = calc_from_time + timedelta(hours=int(service_duration_hours),
                                                                                  minutes=int(service_duration_mins))
                                        if (serv_to_time <= calc_to_time):
                                            if sign == '+':
                                                reserved_from_time = calc_from_time - timedelta(hours=int(hour),
                                                                                                minutes=int(minute))
                                            else:
                                                reserved_from_time = calc_from_time + timedelta(hours=int(hour),
                                                                                                minutes=int(minute))
                                            reserved_to_time = reserved_from_time + timedelta(hours=int(service_duration_hours),
                                                                                              minutes=int(
                                                                                                  service_duration_mins))

                                            reserved_ids = self.env['spa.reservation.line'].search(
                                                [('state', '=', 'order_confirm'), ('employee', '=', employee.id), '|', '&',
                                                 ('start_time', '<=', str(reserved_from_time)),
                                                 ('end_time', '>', str(reserved_from_time)), '&',
                                                 ('start_time', '>=', str(reserved_from_time)),
                                                 ('start_time', '<=', str(reserved_to_time))])
                                            if not reserved_ids:
                                                time_slots.append(calc_from_time)
                                                calc_from_time = calc_from_time + timedelta(minutes=60)
                                                break
                                        else:
                                            break;
                        else:
                            break;

                    if time_slots:
                        slots = []
                        for j in time_slots:
                            p = str(j)
                            slots.append(p[11:16])
                        beautician_dict = {'employee': employee.name, 'employee_id':employee.id,'slots': slots}
                        beautician_list.append(beautician_dict)
            return {
                'status':200,
                'description':'success',
                'beautician':beautician_list
            }

     #   except:
      #      return {
       #         'status': 404,
        #        'description': 'Fail'
         #   }


    def create_spa_booking(self,service_id,reservation_date,employee_id,user_id,start_time):
        try:
            service_obj = self.env['spa.services'].search([('id','=',int(service_id))])
            partner_id = self.env['res.users'].search([('id','=',user_id)])[0].partner_id.id
            if service_obj:
                str_start_time = reservation_date + ' ' + start_time + ':00'
                start = datetime.strptime(str_start_time, '%Y-%m-%d %H:%M:%S')
                end = start + timedelta(hours = service_obj.hours,minutes = service_obj.mins )
                str_end =str(end)
                user_tz = self.search([('id', '=', 2)])[0].tz
                if user_tz:
                    s = datetime.now(pytz.timezone(user_tz)).strftime('%z')
                    sign = s[0]
                    hour = s[1:3]
                    minute = s[3:]
                else:
                    hour = 3
                    minute = 0
                    sign = '+'

                if sign == '+':
                    reserved_from_time = start - timedelta(hours=int(hour), minutes=int(minute))
                    reserved_to_time = end - timedelta(hours=int(hour), minutes=int(minute))
                else:
                    reserved_from_time = start + timedelta(hours=int(hour), minutes=int(minute))
                    reserved_to_time = end + timedelta(hours=int(hour), minutes=int(minute))

                reserved_ids = self.env['spa.reservation.line'].search(
                    [('state', '=', 'order_confirm'), ('employee', '=', employee_id), '|', '&',
                     ('start_time', '<=', str(reserved_from_time)), ('end_time', '>', str(reserved_from_time)), '&',
                     ('start_time', '>=', str(reserved_from_time)), ('start_time', '<=', str(reserved_to_time))])
                if not reserved_ids:
                    self.env['spa.reservation.line'].create({'partner_id':int(partner_id),
                                                             'product_id': service_obj[0].id,
                                                             'price_unit': service_obj[0].list_price,
                                                             'reserve_date': reservation_date,
                                                             'hours': service_obj[0].hours,
                                                             'mins': service_obj[0].mins,
                                                             'employee':int(employee_id),
                                                             'reserve_start':start_time,
                                                             'reserve_end':str_end[11:16],
                                                             'start_time':reserved_from_time,
                                                             'end_time':reserved_to_time

                                                             })
                    return {
                        'status':200,
                        'description':'Booking created Successfully'
                    }
                else:
                    return {
                        'status': 400,
                        'description': 'Timeslot is taken by another user'
                    }


        except:
            return {
            'status': 404,
            'description': 'Fail'
            }


    def get_all_spa_services(self):
        try:

            services = self.env['spa.services'].search([])
            service_list = []
            for service in services:
                vals = {
                    'id': service.id,
                    'name': service.name,
                    'price': service.list_price,
                    'image': service.image_medium,
                    'category_id': service.categ_id and service.categ_id.id or False,
                    'category_name': service.categ_id and service.categ_id.name or False,
                    'description': service.description,
                    'service_hours': service.hours,
                    'service_mins': service.mins,
                    'service_image':service.service_image or ''
                }
                service_list.append(vals)
            return {
                'status': 200,
                'description': 'Success',
                'services': service_list

            }

        except:
            return {
                'status': 404,
                'description': 'Fail'
            }
