# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as dt
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as dt1
from odoo.exceptions import ValidationError, UserError
from odoo.addons import decimal_precision as dp
import math
import pytz

SHIFT_TIMING = [('00:00', '00:00'), ('00:05', '00:05'), ('00:10', '00:10'), ('00:15', '00:15'), ('00:20', '00:20'),
                ('00:25', '00:25'), ('00:30', '00:30'), ('00:35', '00:35'), ('00:40', '00:40'), ('00:45', '00:45'),
                ('00:50', '00:50'), ('00:55', '00:55'), ('01:00', '01:00'), ('01:05', '01:05'), ('01:10', '01:10'),
                ('01:15', '01:15'), ('01:20', '01:20'), ('01:25', '01:25'), ('01:30', '01:30'), ('01:35', '01:35'),
                ('01:40', '01:40'), ('01:45', '01:45'), ('01:50', '01:50'), ('01:55', '01:55'), ('02:00', '02:00'),
                ('02:05', '02:05'), ('02:10', '02:10'), ('02:15', '02:15'), ('02:20', '02:20'), ('02:25', '02:25'),
                ('02:30', '02:30'), ('02:35', '02:35'), ('02:40', '02:40'), ('02:45', '02:45'), ('02:50', '02:50'),
                ('02:55', '02:55'), ('03:00', '03:00'), ('03:05', '03:05'), ('03:10', '03:10'), ('03:15', '03:15'),
                ('03:20', '03:20'), ('03:25', '03:25'), ('03:30', '03:30'), ('03:35', '03:35'), ('03:40', '03:40'),
                ('03:45', '03:45'), ('03:50', '03:50'), ('03:55', '03:55'), ('04:00', '04:00'), ('04:05', '04:05'),
                ('04:10', '04:10'), ('04:15', '04:15'), ('04:20', '04:20'), ('04:25', '04:25'), ('04:30', '04:30'),
                ('04:35', '04:35'), ('04:40', '04:40'), ('04:45', '04:45'), ('04:50', '04:50'), ('04:55', '04:55'),
                ('05:00', '05:00'), ('05:05', '05:05'), ('05:10', '05:10'), ('05:15', '05:15'), ('05:20', '05:20'),
                ('05:25', '05:25'), ('05:30', '05:30'), ('05:35', '05:35'), ('05:40', '05:40'), ('05:45', '05:45'),
                ('05:50', '05:50'), ('05:55', '05:55'), ('06:00', '06:00'), ('06:05', '06:05'), ('06:10', '06:10'),
                ('06:15', '06:15'), ('06:20', '06:20'), ('06:25', '06:25'), ('06:30', '06:30'), ('06:35', '06:35'),
                ('06:40', '06:40'), ('06:45', '06:45'), ('06:50', '06:50'), ('06:55', '06:55'), ('07:00', '07:00'),
                ('07:05', '07:05'), ('07:10', '07:10'), ('07:15', '07:15'), ('07:20', '07:20'), ('07:25', '07:25'),
                ('07:30', '07:30'), ('07:35', '07:35'), ('07:40', '07:40'), ('07:45', '07:45'), ('07:50', '07:50'),
                ('07:55', '07:55'), ('08:00', '08:00'), ('08:05', '08:05'), ('08:10', '08:10'), ('08:15', '08:15'),
                ('08:20', '08:20'), ('08:25', '08:25'), ('08:30', '08:30'), ('08:35', '08:35'), ('08:40', '08:40'),
                ('08:45', '08:45'), ('08:50', '08:50'), ('08:55', '08:55'), ('09:00', '09:00'), ('09:05', '09:05'),
                ('09:10', '09:10'), ('09:15', '09:15'), ('09:20', '09:20'), ('09:25', '09:25'), ('09:30', '09:30'),
                ('09:35', '09:35'), ('09:40', '09:40'), ('09:45', '09:45'), ('09:50', '09:50'), ('09:55', '09:55'),
                ('10:00', '10:00'), ('10:05', '10:05'), ('10:10', '10:10'), ('10:15', '10:15'), ('10:20', '10:20'),
                ('10:25', '10:25'), ('10:30', '10:30'), ('10:35', '10:35'), ('10:40', '10:40'), ('10:45', '10:45'),
                ('10:50', '10:50'), ('10:55', '10:55'), ('11:00', '11:00'), ('11:05', '11:05'), ('11:10', '11:10'),
                ('11:15', '11:15'), ('11:20', '11:20'), ('11:25', '11:25'), ('11:30', '11:30'), ('11:35', '11:35'),
                ('11:40', '11:40'), ('11:45', '11:45'), ('11:50', '11:50'), ('11:55', '11:55'), ('12:00', '12:00'),
                ('12:05', '12:05'), ('12:10', '12:10'), ('12:15', '12:15'), ('12:20', '12:20'), ('12:25', '12:25'),
                ('12:30', '12:30'), ('12:35', '12:35'), ('12:40', '12:40'), ('12:45', '12:45'), ('12:50', '12:50'),
                ('12:55', '12:55'), ('13:00', '13:00'), ('13:05', '13:05'), ('13:10', '13:10'), ('13:15', '13:15'),
                ('13:20', '13:20'), ('13:25', '13:25'), ('13:30', '13:30'), ('13:35', '13:35'), ('13:40', '13:40'),
                ('13:45', '13:45'), ('13:50', '13:50'), ('13:55', '13:55'), ('14:00', '14:00'), ('14:05', '14:05'),
                ('14:10', '14:10'), ('14:15', '14:15'), ('14:20', '14:20'), ('14:25', '14:25'), ('14:30', '14:30'),
                ('14:35', '14:35'), ('14:40', '14:40'), ('14:45', '14:45'), ('14:50', '14:50'), ('14:55', '14:55'),
                ('15:00', '15:00'), ('15:05', '15:05'), ('15:10', '15:10'), ('15:15', '15:15'), ('15:20', '15:20'),
                ('15:25', '15:25'), ('15:30', '15:30'), ('15:35', '15:35'), ('15:40', '15:40'), ('15:45', '15:45'),
                ('15:50', '15:50'), ('15:55', '15:55'), ('16:00', '16:00'), ('16:05', '16:05'), ('16:10', '16:10'),
                ('16:15', '16:15'), ('16:20', '16:20'), ('16:25', '16:25'), ('16:30', '16:30'), ('16:35', '16:35'),
                ('16:40', '16:40'), ('16:45', '16:45'), ('16:50', '16:50'), ('16:55', '16:55'), ('17:00', '17:00'),
                ('17:05', '17:05'), ('17:10', '17:10'), ('17:15', '17:15'), ('17:20', '17:20'), ('17:25', '17:25'),
                ('17:30', '17:30'), ('17:35', '17:35'), ('17:40', '17:40'), ('17:45', '17:45'), ('17:50', '17:50'),
                ('17:55', '17:55'), ('18:00', '18:00'), ('18:05', '18:05'), ('18:10', '18:10'), ('18:15', '18:15'),
                ('18:20', '18:20'), ('18:25', '18:25'), ('18:30', '18:30'), ('18:35', '18:35'), ('18:40', '18:40'),
                ('18:45', '18:45'), ('18:50', '18:50'), ('18:55', '18:55'), ('19:00', '19:00'), ('19:05', '19:05'),
                ('19:10', '19:10'), ('19:15', '19:15'), ('19:20', '19:20'), ('19:25', '19:25'), ('19:30', '19:30'),
                ('19:35', '19:35'), ('19:40', '19:40'), ('19:45', '19:45'), ('19:50', '19:50'), ('19:55', '19:55'),
                ('20:00', '20:00'), ('20:05', '20:05'), ('20:10', '20:10'), ('20:15', '20:15'), ('20:20', '20:20'),
                ('20:25', '20:25'), ('20:30', '20:30'), ('20:35', '20:35'), ('20:40', '20:40'), ('20:45', '20:45'),
                ('20:50', '20:50'), ('20:55', '20:55'), ('21:00', '21:00'), ('21:05', '21:05'), ('21:10', '21:10'),
                ('21:15', '21:15'), ('21:20', '21:20'), ('21:25', '21:25'), ('21:30', '21:30'), ('21:35', '21:35'),
                ('21:40', '21:40'), ('21:45', '21:45'), ('21:50', '21:50'), ('21:55', '21:55'), ('22:00', '22:00'),
                ('22:05', '22:05'), ('22:10', '22:10'), ('22:15', '22:15'), ('22:20', '22:20'), ('22:25', '22:25'),
                ('22:30', '22:30'), ('22:35', '22:35'), ('22:40', '22:40'), ('22:45', '22:45'), ('22:50', '22:50'),
                ('22:55', '22:55'), ('23:00', '23:00'), ('23:05', '23:05'), ('23:10', '23:10'), ('23:15', '23:15'),
                ('23:20', '23:20'), ('23:25', '23:25'), ('23:30', '23:30'), ('23:35', '23:35'), ('23:40', '23:40'),
                ('23:45', '23:45'), ('23:50', '23:50'), ('23:55', '23:55')]


class SpaFolio(models.Model):
    _inherit = 'spa.folio'
    _order = 'reservation_id desc'

    reservation_id = fields.Many2one('spa.reservation',
                                     string='Reservation Id')

    @api.multi
    def write(self, vals):
        context = dict(self._context)
        if not context:
            context = {}
        context.update({'from_reservation': True})
        res = super(SpaFolio, self).write(vals)
        reservation_line_obj = self.env['spa.employee.reservation.line']
        for folio_obj in self:
            if folio_obj.reservation_id:
                for reservation in folio_obj.reservation_id:
                    reservation_obj = (reservation_line_obj.search
                                       ([('reserve_id', '=',
                                          reservation.id)]))
                    if len(reservation_obj) == 1:
                        for line in reservation.reservation_line:
                            vals = {'employee_id': line.employee.id,
                                    'start_time': line.start_time,
                                    'end_time': line.end_time,
                                    'state': 'assigned',
                                    'reservation_id': reservation.id,
                                    }
                            reservation_obj.write(vals)
        return res


class SpaReservation(models.Model):
    _name = "spa.reservation"
    _rec_name = "reservation_no"
    _description = "Reservation"
    _order = 'reservation_no desc'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']

    reservation_no = fields.Char('Reservation No', size=64, readonly=True)
    date_order = fields.Datetime('Booking Date', readonly=True, required=True,
                                 index=True,
                                 default=(lambda *a: time.strftime(dt)))
    warehouse_id = fields.Many2one('stock.warehouse', 'Spa', readonly=True,
                                   index=True,
                                   required=True, default=1,
                                   states={'draft': [('readonly', False)]})
    partner_id = fields.Many2one('res.partner', 'Guest Name', readonly=True,
                                 index=True,
                                 required=True,
                                 states={'draft': [('readonly', False)]})
    pricelist_id = fields.Many2one('product.pricelist', 'Scheme',
                                   required=True, readonly=True,
                                   states={'draft': [('readonly', False)]},
                                   help="Pricelist for current reservation.")
    partner_invoice_id = fields.Many2one('res.partner', 'Invoice Address',
                                         readonly=True,
                                         states={'draft':
                                                     [('readonly', False)]},
                                         help="Invoice address for "
                                              "current reservation.")
    partner_order_id = fields.Many2one('res.partner', 'Ordering Contact',
                                       readonly=True,
                                       states={'draft':
                                                   [('readonly', False)]},
                                       help="The name and address of the "
                                            "contact that requested the order "
                                            "or quotation.")
    partner_shipping_id = fields.Many2one('res.partner', 'Delivery Address',
                                          readonly=True,
                                          states={'draft':
                                                      [('readonly', False)]},
                                          help="Delivery address"
                                               "for current reservation. ")
    reservation_line = fields.One2many('spa.reservation.line', 'reservation_id',
                                       string='Reservation Line',
                                       help='Spa reservation details.',
                                       readonly=True,
                                       states={'draft': [('readonly', False)]},
                                       )
    state = fields.Selection([('draft', 'Draft'), ('confirm', 'Confirm'),
                              ('cancel', 'Cancel'), ('done', 'Done')],
                             'State', readonly=True,
                             default=lambda *a: 'draft')
    folio_id = fields.Many2many('spa.folio', 'spa_folio_reservation_rel',
                                'order_id', 'invoice_id', string='Folio')
    no_of_folio = fields.Integer('Folio', compute="_compute_folio_id")
    dummy = fields.Datetime('Dummy')
    source = fields.Selection([('direct', 'Direct'),
                               ('mobile', 'Mobile'), ('web', 'Website')], 'Booking type', default='direct')

    paid = fields.Boolean(string="Paid", default=False)

    @api.multi
    def _compute_folio_id(self):
        folio_list = []
        for res in self:
            for folio in res.folio_id:
                folio_list.append(folio.id)
            folio_len = len(folio_list)
            res.no_of_folio = folio_len
        return folio_len

    @api.multi
    def unlink(self):
        """
        Overrides orm unlink method.
        @param self: The object pointer
        @return: True/False.
        """
        for reserv_rec in self:
            if reserv_rec.state != 'draft':
                raise ValidationError(_('You cannot delete Reservation in %s\
                                         state.') % (reserv_rec.state))
        return super(SpaReservation, self).unlink()

    @api.multi
    def copy(self):
        ctx = dict(self._context) or {}
        ctx.update({'duplicate': True})
        return super(SpaReservation, self.with_context(ctx)).copy()

    @api.constrains('reservation_line')
    def check_reservation_services(self):
        '''
        This method is used to validate the reservation_line.
        -----------------------------------------------------
        @param self: object pointer
        @return: raise a warning depending on the validation
        '''
        for reservation in self:
            if len(reservation.reservation_line) == 0:
                raise ValidationError(_('Please Select a Service \
                    For Reservation.'))

    @api.onchange('reservation_line')
    def onchange_reservation_line(self):

        context = self.env.context

        en_time = ""

        # if 'reservation_line' in context:
        # for item in context['reservation_line']:
        # if item[2] != False:
        # if en_time < item[2]['end_time']:
        # en_time = item[2]['end_time']
        # else:
        # if en_time < self.env['spa.reservation.line'].browse(item[1]).end_time:
        # en_time = self.env['spa.reservation.line'].browse(item[1]).end_time

        # if en_time != "":
        # self.end_time = en_time

    @api.model
    def _needaction_count(self, domain=None):
        """
         Show a count of draft state reservations on the menu badge.
         """
        return self.search_count([('state', '=', 'draft')])

    @api.multi
    def onchange_check_times(self, start_time=False, end_time=False,
                             duration=False):
        '''
        --------------------------------------------------------------------
        @param self: object pointer
        @return: Duration and checkout_date
        '''
        value = {}

        duration = 0
        if start_time and end_time:
            chkin_dt = (datetime.strptime
                        (start_time, dt))
            chkout_dt = (datetime.strptime
                         (end_time, dt))
            dur = chkout_dt - chkin_dt
            sec_dur = dur.seconds
            myduration = abs((dur.seconds / 60) / 60)
        value.update({'duration': myduration})
        return value

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        '''
        When you change partner_id it will update the partner_invoice_id,
        partner_shipping_id and pricelist_id of the hotel reservation as well
        ---------------------------------------------------------------------
        @param self: object pointer
        '''
        if not self.partner_id:
            self.partner_invoice_id = False
            self.partner_shipping_id = False
            self.partner_order_id = False
        else:
            addr = self.partner_id.address_get(['delivery', 'invoice',
                                                'contact'])
            self.partner_invoice_id = addr['invoice']
            self.partner_order_id = addr['contact']
            self.partner_shipping_id = addr['delivery']
            self.pricelist_id = self.partner_id.property_product_pricelist.id

    @api.model
    def create(self, vals):
        """
        Overrides orm create method.
        @param self: The object pointer
        @param vals: dictionary of fields value.
        """
        if not vals:
            vals = {}
        vals['reservation_no'] = self.env['ir.sequence']. \
                                     next_by_code('spa.reservation') or 'New'
        if 'reservation_line' in vals.keys():
            lines = [l[2] for l in vals.get('reservation_line')]
            #            end_time = datetime.strptime(lines[-1]['end_time'],
            #                                              DEFAULT_SERVER_DATETIME_FORMAT)

            for l in lines:

                start = datetime.strptime(l['start_time'],
                                          dt)
                end = datetime.strptime(l['end_time'],
                                        dt)
                dur = relativedelta(end, start)
                if dur.days:
                    product = self.env['product.product'].browse([l['product_id']])
                    raise ValidationError(_('Days reservation not permitted.'))

                    #             vals['end_time'] = end_time

        res = super(SpaReservation, self).create(vals)
        if res.reservation_line:
            for line in res.reservation_line:
                if not line.partner_id:
                    line.partner_id = res.partner_id.id
        return res

    @api.multi
    def check_overlap(self, date1, date2):
        date2 = datetime.strptime(date2, '%Y-%m-%d')
        date1 = datetime.strptime(date1, '%Y-%m-%d')
        delta = date2 - date1
        return set([date1 + timedelta(days=i) for i in range(delta.days + 1)])

    @api.multi
    def confirmed_reservation(self):
        """
        This method create a new recordset for spa employee reservation line
        ------------------------------------------------------------------
        @param self: The object pointer
        @return: new record set for spa employee reservation line.
        """
        reservation_line_obj = self.env['spa.employee.reservation.line']
        flag = True
        for reservation in self.reservation_line:
            if reservation.employee and reservation.product_id:
                assigned = False
                for line in reservation.employee.employee_reservation_line_ids:
                    if line.status != 'cancel':
                        if (reservation.start_time <= line.start_time <=
                                reservation.end_time) or (reservation.start_time <=
                                                              line.end_time <=
                                                              reservation.end_time):
                            assigned = True
                for rm_line in reservation.employee.spa_service_line_ids:
                    if rm_line.status != 'cancel':
                        if (reservation.start_time <= rm_line.ser_start_time <=
                                reservation.end_time) or (reservation.start_time <=
                                                              rm_line.ser_end_time <=
                                                              reservation.end_time):
                            assigned = True
                if assigned:
                    raise ValidationError(
                        _('Employee reserved for the same time slot more than once,\
                                      please check n confirm'))
            roomcount = 0.0
            if roomcount:
                raise except_orm(_('Warning'), _('You tried to confirm \
                    reservation with employee those already reserved in this \
                    reservation period'))
                flag = False
            else:
                reservation.write({'state': 'order_confirm'})
                if reservation.employee:
                    vals = {
                        'employee_id': reservation.employee.id,
                        'start_time': reservation.start_time,
                        'end_time': reservation.end_time,
                        'state': 'assigned',
                        'reserve_id': reservation.reservation_id.id,
                        'product_id': reservation.product_id.id
                    }
                    reservation.employee.write({'isemployee': False, 'status': 'occupied'})
                    reservation_line_obj.create(vals)

        if flag == True:
            self.write({'state': 'confirm'})
        return True

    @api.multi
    def cancel_reservation(self):
        """
        This method cancel recordset for spa employee reservation line
        ------------------------------------------------------------------
        @param self: The object pointer
        @return: cancel record set for spa employee reservation line.
        """
        emp_res_line_obj = self.env['spa.employee.reservation.line']
        spa_res_line_obj = self.env['spa.reservation.line']
        self.write({'state': 'cancel'})
        emp_reservation_line = emp_res_line_obj.search([('reserve_id',
                                                         'in', self.ids)])
        emp_reservation_line.write({'state': 'unassigned'})
        reservation_lines = spa_res_line_obj.search([('reservation_id',
                                                      'in', self.ids)])
        for reservation_line in reservation_lines:
            reservation_line.employee.write({'isemployee': True,
                                             'status': 'available'})
        return True

    @api.multi
    def set_to_draft_reservation(self):
        self.state = 'draft'
        return True

    @api.multi
    def send_reservation_mail(self):
        '''
        This function opens a window to compose an email,
        template message loaded by default.
        @param self: object pointer
        '''
        assert len(self._ids) == 1, 'This is for a single id at a time.'
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = (ir_model_data.get_object_reference
                           ('spa_reservation',
                            'email_template_spa_reservation')[1])
        except ValueError:
            template_id = False
        try:
            compose_form_id = (ir_model_data.get_object_reference
                               ('mail',
                                'email_compose_message_wizard_form')[1])
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'spa.reservation',
            'default_res_id': self._ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'force_send': True,
            'mark_so_as_sent': True
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
            'force_send': True
        }

    @api.model
    def _reservation_reminder_24hrs(self):
        """
        This method is for scheduler
        every 1day scheduler will call this method to
        find all tomorrow's reservations.
        ----------------------------------------------
        @param self: The object pointer
        @return: send a mail
        """
        now_str = time.strftime(dt)
        now_date = datetime.strptime(now_str,
                                     dt)
        ir_model_data = self.env['ir.model.data']
        template_id = (ir_model_data.get_object_reference
                       ('spa_reservation',
                        'email_template_reservation_reminder_24hrs')[1])
        template_rec = self.env['mail.template'].browse(template_id)
        for travel_rec in self.search([('state', 'in', ['confirm', 'done'])]):
            for rsv in travel_rec.reservation_line:
                start_time = (datetime.strptime
                              (rsv.start_time,
                               dt))
                difference = relativedelta(now_date, start_time)
                if (difference.days == -1 and travel_rec.partner_id.email):
                    template_rec.send_mail(travel_rec.id, force_send=True)
        return True

    @api.model
    def _check_booking_status(self):

        now_str = time.strftime(dt)
        now_date = datetime.strptime(now_str,
                                     dt)
        for booking_rec in self.search([('state', 'in', ['confirm'])]):
            for rsv in booking_rec.reservation_line:
                start_time = (datetime.strptime
                              (str(rsv.start_time),
                               dt))
                difference = relativedelta(now_date, start_time)
                if (difference.minutes > 10) and (booking_rec.paid == False):
                    booking_rec.unlink()
        return True

    @api.multi
    def create_folio(self):
        """
        This method is for create new spa folio.
        -----------------------------------------
        @param self: The object pointer
        @return: new record set for spa folio.
        """
        spa_folio_obj = self.env['spa.folio']
        emp_obj = self.env['spa.employee']
        service_obj = self.env['spa.services']
        for reservation in self:
            service_lines = []

            folio_vals = {
                'date_order': fields.Date.today(),
                'warehouse_id': reservation.warehouse_id.id,
                'partner_id': reservation.partner_id.id,
                'pricelist_id': reservation.pricelist_id.id,
                'partner_invoice_id': reservation.partner_invoice_id.id,
                'partner_shipping_id': reservation.partner_shipping_id.id,
                'reservation_id': reservation.id,
                'booking_flag': True,
            }
            for line in reservation.reservation_line:
                prod = self.env['spa.services'].search([('id', '=', line.product_id.id)]).service_id or False
                partner = reservation.partner_id.id
                price_list = reservation.pricelist_id.id
                service_line_obj = self.env['spa.service.line']
                prod = prod.with_context(
                    lang=False,
                    partner=partner,
                    quantity=1.0,
                    date=self.date_order,
                    pricelist=False,
                    uom=line.product_uom.id
                )
                prod_uom = prod.uom_id.id
                price_unit = line.price_unit

                date_a = (datetime
                          (*time.strptime(str(line.end_time), dt)[:5]))
                date_b = (datetime
                          (*time.strptime(str(line.start_time),
                                          dt)[:5]))

                service_lines.append((0, 0, {
                    'ser_start_time': str(line.start_time),
                    'ser_end_time': str(line.end_time),
                    'product_id': prod.id,
                    'name': reservation.reservation_no,
                    'product_uom': prod_uom,
                    'price_unit': price_unit,
                    'product_uom_qty': ((date_a - date_b).days) + 1,
                    'is_reserved': True,
                    'employee_id': line.employee.id}))
                res_obj = emp_obj.browse([line.employee.id])
                res_obj.write({'status': 'occupied', 'isemployee': False})
            folio_vals.update({'service_lines': service_lines})
            folio = spa_folio_obj.create(folio_vals)
            self._cr.execute('insert into spa_folio_reservation_rel'
                             '(order_id, invoice_id) values (%s,%s)',
                             (reservation.id, folio.id)
                             )
            reservation.write({'state': 'done'})
        return folio


class SpaReservationLine(models.Model):
    _name = "spa.reservation.line"
    _description = "Reservation Line"

    def days_between(self, d1, d2):
        d1 = datetime.strptime(d1, "%Y-%m-%d")
        d2 = datetime.strptime(d2, "%Y-%m-%d")
        return abs((d2 - d1).days)

    @api.multi
    def _invoice_count(self):
        for rec in self:
            c = 0
            if rec.reservation_id:
                c = 1
        return c

    @api.multi
    def close_reservation(self):
        return {
            'type': 'ir.actions.client',
            'tag': 'booking.calendar',
        }

    @api.model
    def calendar_reservation(self):
        # change start_time
        user_time_zone = pytz.UTC
        if self._context.get('tz'):
            # change the timezone to the timezone of the user
            user_time_zone = pytz.timezone(self._context.get('tz'))
        # create a time object
        reservation_start = str(self._context.get('reservation_start'))
        reservation_start = reservation_start.replace('T', ' ')
        user_time = datetime.strptime(reservation_start, "%Y-%m-%d %H:%M:%S")
        # define the timezone of the time object
        user_time = user_time_zone.localize(user_time)
        user_time.replace(tzinfo=user_time_zone)

        start_time = user_time.astimezone(pytz.UTC).strftime(dt)
        diff = self.days_between(user_time.strftime(dt1), user_time.astimezone(pytz.UTC).strftime(dt1))
        # print(diff,' diff')
        if diff:
            # change reserve_end
            change_st = datetime.strptime(start_time, dt)
            change_st = change_st + timedelta(days=diff)
            start_time = datetime.strftime(change_st, dt)
        return start_time

    @api.model
    def _reservation_start_time(self):
        if self._context.get('reservation_start', False):
            return self.calendar_reservation()
        return time.strftime(dt)

    @api.model
    def _reservation_date(self):
        if self._context.get('reservation_start', False):
            reserve_date = self.calendar_reservation()
            return reserve_date[:10]
        return time.strftime(dt)[:10]

    @api.model
    def _reservation_start(self):
        cur_time = time.strftime(dt)
        starting_hrs = cur_time[11:13]
        starting_mins = int(5 * round(float(cur_time[14:16]) / 5))
        starting_time = str(starting_hrs) + ':' + str('%02d' % starting_mins)
        return starting_time

    name = fields.Char('Name', size=64)
    reservation_id = fields.Many2one('spa.reservation',
                                     string='Reservation', ondelete='cascade', )
    product_id = fields.Many2one('spa.services', string="Service", required=True)
    partner_id = fields.Many2one('res.partner', string="Customer", domain="[('customer','=',True)]")
    booking_date = fields.Datetime(related="reservation_id.date_order", string="Customer")

    categ_id = fields.Many2one(related="product_id.categ_id",
                               string="Category", required=True)
    employee = fields.Many2one('spa.employee', string="Employee", required=True, )

    reserve_date = fields.Date('Reservation Date', required=True, default=_reservation_date, )
    reserve_start = fields.Selection(SHIFT_TIMING, 'Start Time', required=True)
    reserve_end = fields.Selection(SHIFT_TIMING, 'End Time', required=True, )
    start_time = fields.Datetime('Start time', required=True, default=_reservation_start_time, )
    end_time = fields.Datetime('End Time', required=True, )

    hours = fields.Integer(string="Hrs", required=True, )
    mins = fields.Integer(string="Mins", required=True, )
    price_unit = fields.Float('Unit Price', required=True, digits_compute=dp.get_precision('Product Price'))
    product_uom = fields.Many2one('product.uom', 'Unit of Measure ')

    status = fields.Selection(string='state', related='reservation_id.state')
    reserved = fields.Boolean(string="Reserved")
    state = fields.Selection([('draft', 'Draft'), ('order_confirm', 'Appointment Confirmed'),
                              ('cancel', 'Cancel'), ('done', 'Done')],
                             'State', readonly=True,
                             default=lambda *a: 'draft')
    color = fields.Integer('Color Index', default=0)
    invoice_number = fields.Many2one('account.invoice', string="Invoice Number")
    invoice_status = fields.Selection(related='reservation_id.folio_id.invoice_status', string="Invoiced",
                                      readonly=True)

    invoice_count = fields.Integer(compute='_invoice_count', string='# Sales')


    @api.depends('invoice_status')
    @api.onchange('invoice_status')
    def _onchange_inv_status(self):
        if self.invoice_status == 'done':
            self.state = 'done'

    @api.onchange('reserve_start', 'reserve_end')
    def onchange_reserve_start(self):
        if not self.reserve_date:
            self.reserve_start = False
            self.reserve_end = False
            raise ValidationError(
                _('Please choose the Reservation Date and continue.'))
        if self.reserve_date and self.reserve_start:
            if not self.product_id and self.employee:
                self.reserve_start = False
                self.reserve_end = False
                raise ValidationError(
                    _('Please choose the Service and continue.'))

            reserve_start = self.reserve_start.split(':')
            ending_hrs = int(5 * round(float(reserve_start[1]) / 5))
            start_time = str(self.reserve_date) + ' ' + str(reserve_start[0]) + ':' + str('%02d' % ending_hrs) + ':00'
            # change reserve_end
            change_et = datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S")
            change_et = change_et + timedelta(hours=self.product_id.hours, minutes=self.product_id.mins)
            end_time_tz = datetime.strftime(change_et, "%Y-%m-%d %H:%M:%S")

            self.reserve_end = str(end_time_tz)[11:16]

            # change start_time
            user_time_zone = pytz.UTC
            if self._context.get('tz'):
                # change the timezone to the timezone of the user
                user_time_zone = pytz.timezone(self._context.get('tz'))
            # create a time object
            user_time = datetime.strptime(start_time, dt)
            # define the timezone of the time object
            user_time = user_time_zone.localize(user_time)
            user_time.replace(tzinfo=user_time_zone)
            diff = self.days_between(start_time[:10], user_time.astimezone(pytz.UTC).strftime(dt1))
            # print (diff,' diff ',dt1,' dt1 ',dt,' dt ',start_time[:10],' start_time ',user_time.astimezone(pytz.UTC).strftime(dt1))
            if diff:
                user_tz = user_time.astimezone(pytz.UTC).strftime(dt)
                change_st = str(start_time[:10]) + ' ' + str(user_tz[11:19])
                self.start_time = change_st
            else:
                self.start_time = user_time.astimezone(pytz.UTC).strftime(dt)
            self.on_change_times()

    @api.multi
    def close_dialog(self):
        return {'type': 'ir.actions.act_window_close'}

    @api.onchange('start_time', 'end_time')
    def on_change_times(self):
        if self.product_id:
            # set end time from services
            change_et = datetime.strptime(str(self.start_time), "%Y-%m-%d %H:%M:%S")
            change_et = change_et + timedelta(hours=self.product_id.hours, minutes=self.product_id.mins)
            self.end_time = datetime.strftime(change_et, "%Y-%m-%d %H:%M:%S")
            if self.end_time < self.start_time:
                raise ValidationError(
                    _('Endtime should be greater.'))

            spa_employee_obj = self.env['spa.employee']
            spa_employee_ids = spa_employee_obj.search([('allowed_services', '=', self.product_id.id)])
            emp_ids = []
            # if not self.reservation_id.partner_id:
            #     raise ValidationError(
            #                      _('Before choosing a service,\n You have to choose \
            #                          a partner in\
            #                          the reservation form.'))
            # get timing difference
            if self.start_time and self.end_time:
                start_time = datetime.strptime(str(self.start_time), "%Y-%m-%d %H:%M:%S")
                end_time = datetime.strptime(str(self.end_time), "%Y-%m-%d %H:%M:%S")
                diff = end_time - start_time
                days, seconds = diff.days, diff.seconds
                self.hours = days * 24 + seconds // 3600
                self.mins = (seconds % 3600) // 60
                for emp in spa_employee_ids:
                    assigned = False
                    buffer_time = self.float_time_convert(self.env.user.company_id.spa_buffer_time)
                    for line in emp.employee_reservation_line_ids:
                        if line.status != 'cancel':
                            # buffer_time diff

                            line_et = datetime.strptime(str(line.end_time), "%Y-%m-%d %H:%M:%S")
                            line_et = line_et + timedelta(hours=buffer_time[0], minutes=buffer_time[1])
                            buf_line_et = datetime.strftime(line_et, "%Y-%m-%d %H:%M:%S")
                            # print line.start_time,' line.start_time ',line.end_time,' line.end_time ',buf_line_et,' buf_line_et ',self.start_time,' self.start_time',self.end_time,' self.end_time'
                            if (str(line.start_time) <= str(self.start_time) <=
                                    buf_line_et) or (str(line.start_time) <= str(self.end_time) <=
                                                         buf_line_et) or (
                                    str(self.start_time) <= str(line.start_time) <=
                                str(self.end_time)):
                                assigned = True

                    for rm_line in emp.spa_service_line_ids:
                        if rm_line.status != 'cancel':
                            # buffer_time diff
                            rm_line_et = datetime.strptime(str(rm_line.ser_end_time), "%Y-%m-%d %H:%M:%S")
                            rm_line_et = rm_line_et + timedelta(hours=buffer_time[0], minutes=buffer_time[1])
                            buf_rm_line_et = datetime.strftime(rm_line_et, "%Y-%m-%d %H:%M:%S")
                            if (str(rm_line.ser_start_time) <= str(self.start_time) <=
                                    buf_rm_line_et) or (str(rm_line.ser_start_time) <= str(self.end_time) <=
                                                            buf_rm_line_et) or (
                                    str(self.start_time) <= str(rm_line.ser_start_time) <=
                                str(self.end_time)):
                                assigned = True
                    att_assigned = False
                    emp_att = emp.employee_shift_timing_ids.filtered(lambda att: int(att.name) == start_time.weekday())
                    # checking whether attendance time assigned or not
                    for att in emp_att:
                        att_start_hrs = att.hour_from.split(':')  # self.float_time_convert(att.hour_from)
                        intl_st = str(start_time)[:-8] + str(att_start_hrs[0]) + ':' + str(att_start_hrs[1]) + ':00'
                        intl_st = datetime.strptime(intl_st, dt)
                        start_time_tz = datetime.strptime(str(start_time), dt).replace(tzinfo=pytz.UTC).astimezone(
                            pytz.timezone(self._context.get('tz') or 'UTC'))
                        start_time_tz = datetime.strptime(str(start_time_tz)[:19], dt)

                        att_end_hrs = att.hour_to.split(':')  # self.float_time_convert(att.hour_to)
                        intl_et = str(end_time)[:-8] + str(att_end_hrs[0]) + ':' + str(att_end_hrs[1]) + ':00'
                        intl_et = datetime.strptime(intl_et, dt)
                        end_time_tz = datetime.strptime(str(end_time), dt).replace(tzinfo=pytz.UTC).astimezone(
                            pytz.timezone(self._context.get('tz') or 'UTC'))
                        end_time_tz = datetime.strptime(str(end_time_tz)[:19], dt)
                        # print intl_st,' intl_st ',intl_et,' intl_et ',start_time_tz,' start_time_tz ',end_time_tz,' end_time_tz'
                        if (intl_st <= start_time_tz <=
                                intl_et) or (intl_st <= end_time_tz <=
                                                 intl_et) or (start_time_tz <= intl_st <=
                                                                  end_time_tz):
                            att_assigned = True

                    # leave details check
                    leave_details = emp.resource_id.calendar_id.leave_ids.filtered(
                        lambda att: att.resource_id == emp.resource_id)
                    for leave in leave_details:
                        if (leave.date_from <= self.start_time <=
                                leave.date_to) or (leave.date_from <= self.end_time <=
                                                       leave.date_to) or (self.start_time <= leave.date_from <=
                                                                              self.end_time):
                            att_assigned = False

                    if not assigned and att_assigned:
                        emp_ids.append(emp.id)
                    d_emp = self.env['spa.employee'].search([('name', '=', 'Default Employee')])
                    if d_emp:
                        emp_ids.append(d_emp.id)
            domain = {'employee': [('id', 'in', emp_ids)]}
            return {'domain': domain}

    @api.onchange('product_id')
    def on_change_product_id(self):
        '''
        When you change product_id it check checkin and checkout are
        filled or not if not then raise warning
        -----------------------------------------------------------
        @param self: object pointer
        '''
        if self.product_id:
            prod = self.product_id.id
            partner = self.reservation_id.partner_id
            price_list = self.reservation_id.pricelist_id.id
            service_line_obj = self.env['spa.service.line']
            vals = {}
            domain = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
            if not self.product_uom or (self.product_id.uom_id.id != self.product_uom.id):
                vals['product_uom'] = self.product_id.uom_id
                vals['product_uom_qty'] = 1.0
            vals['partner_id'] = partner
            vals['pricelist'] = price_list
            product = self.product_id.with_context(
                lang=False,
                partner=partner.id,
                quantity=vals.get('product_uom_qty') or self.product_uom_qty,
                date=self.reservation_id.date_order,
                pricelist=False,
                uom=self.product_uom.id
            )

            self.price_unit = product.lst_price

        hrs = int(self.product_id.hours)
        mins = int(self.product_id.mins)
        start_time = datetime.strptime(str(self.start_time), "%Y-%m-%d %H:%M:%S")
        self.end_time = start_time + timedelta(hours=hrs, minutes=mins)
        end_time = datetime.strptime(str(self.end_time), "%Y-%m-%d %H:%M:%S")
        self.reserve_date = str(self.start_time)[:10]
        start_time_tz = datetime.strptime(str(start_time), dt).replace(tzinfo=pytz.UTC).astimezone(
            pytz.timezone(self._context.get('tz') or 'UTC'))
        end_time_tz = datetime.strptime(str(end_time), dt).replace(tzinfo=pytz.UTC).astimezone(
            pytz.timezone(self._context.get('tz') or 'UTC'))

        # starting time
        starting_hrs = str(start_time_tz)[11:13]
        starting_mins = int(5 * round(float(str(start_time_tz)[14:16]) / 5))
        if starting_mins == 60:
            starting_hrs = int(starting_hrs) + 1
            starting_hrs = str('%02d' % starting_hrs)
            starting_mins = 0
        starting_time = str(starting_hrs) + ':' + str('%02d' % starting_mins)
        self.reserve_start = starting_time
        # ending time
        ending_hrs = str(end_time_tz)[11:13]
        ending_mins = int(5 * round(float(str(end_time_tz)[14:16]) / 5))
        ending_time = str(ending_hrs) + ':' + str('%02d' % ending_mins)
        self.reserve_end = ending_time

        spa_employee_obj = self.env['spa.employee']
        spa_employee_ids = spa_employee_obj.search([('allowed_services', '=', self.product_id.id)])
        emp_ids = []
        # if not self.reservation_id.partner_id:
        #     raise ValidationError(
        #                      _('Before choosing a service,\n You have to choose \
        #                          a partner in\
        #                          the reservation form.'))
        for emp in spa_employee_ids:
            assigned = False
            for line in emp.employee_reservation_line_ids:
                if line.status != 'cancel':
                    if (line.start_time <= self.start_time <=
                            line.end_time) or (line.start_time <=
                                                   self.end_time <=
                                                   line.end_time):
                        assigned = True
            for rm_line in emp.spa_service_line_ids:
                if rm_line.status != 'cancel':
                    if (rm_line.ser_start_time <= self.start_time <=
                            rm_line.ser_end_time) or (rm_line.ser_start_time <=
                                                          self.end_time <=
                                                          rm_line.ser_end_time):
                        assigned = True
            if not assigned:
                emp_ids.append(emp.id)
            d_emp = self.env['spa.employee'].search([('name', '=', 'Default Employee')])
            if d_emp:
                emp_ids.append(d_emp.id)
        domain = {'employee': [('id', 'in', emp_ids)]}
        return {'domain': domain}

    @api.multi
    def float_time_convert(self, float_val):
        factor = float_val < 0 and -1 or 1
        val = abs(float_val)
        return (factor * int(math.floor(val)), int(round((val % 1) * 60)))

    @api.onchange('employee')
    def on_change_employee(self):
        if self.employee and self.product_id:

            assigned = False
            for line in self.employee.employee_reservation_line_ids:
                if line.status != 'cancel':
                    if (self.start_time <= line.start_time <=
                            self.end_time) or (self.start_time <=
                                                   line.end_time <=
                                                   self.end_time):
                        assigned = True
            for rm_line in self.employee.spa_service_line_ids:
                if rm_line.status != 'cancel':
                    if (self.start_time <= rm_line.ser_start_time <=
                            self.end_time) or (self.start_time <=
                                                   rm_line.ser_end_time <=
                                                   self.end_time):
                        assigned = True
            if assigned:
                raise ValidationError(_('Warning'),
                                      _(
                                          'Employee already reserved for another time slot in this period, please check n confirm'))

    @api.multi
    def unlink(self):
        """
        Overrides orm unlink method.
        @param self: The object pointer
        @return: True/False.
        """
        spa_employee_reserv_line_obj = self.env['spa.employee.reservation.line']
        for reserv_rec in self:
            if reserv_rec.employee:
                hres_arg = [('employee_id', '=', reserv_rec.employee.id),
                            ('reserve_id', '=', reserv_rec.reservation_id.id)]
                myobj = spa_employee_reserv_line_obj.search(hres_arg)
                if myobj.ids:
                    reserv_rec.employee.write({'isemployee': True, 'status': 'available'})
                    myobj.unlink()
        return super(SpaReservationLine, self).unlink()

    # @api.multi
    # def write(self, vals):
    #     context = dict(self._context)
    #     if not context:
    #         context = {}
    #     print "////////////YES WRITE"

    @api.model
    def create(self, vals):
        ctx = self.env.context
        if not vals.get('reservation_id'):
            spa_res_obj = self.env['spa.reservation']
            partner_id = self.env['res.partner'].browse([vals.get('partner_id')])
            addr = partner_id.address_get(['delivery', 'invoice',
                                           'contact'])
            partner_invoice_id = addr['invoice']
            partner_order_id = addr['contact']
            partner_shipping_id = addr['delivery']
            pricelist_id = partner_id.property_product_pricelist.id
            source = 'direct'
            if vals.get('source'):
                source = vals.get('source')
            paid = False
            if vals.get('paid')== True:
                paid = True
            rec = (spa_res_obj.create
                   ({'partner_id': partner_id.id,
                     'partner_invoice_id': partner_invoice_id,
                     'partner_order_id': partner_order_id,
                     'partner_shipping_id': partner_shipping_id,
                     'warehouse_id': 1,
                     'pricelist_id': pricelist_id,
                     'source': source,
                     'paid':paid
                     }))
            vals.update({'reservation_id': rec.id, 'name': rec.reservation_no})

        res = super(SpaReservationLine, self).create(vals)
        res.create_folio()

        return res

    @api.multi
    def create_folio(self):
        if self.reservation_id:
            self.reservation_id.confirmed_reservation()
            if not self.reservation_id.folio_id:
                folio = self.reservation_id.create_folio()
                # folio.action_confirm()
                self.state = 'order_confirm'

    @api.multi
    def view_folio(self):
        if self.reservation_id and self.reservation_id.folio_id:
            folio = self.reservation_id.folio_id
            return {
                'view_type': 'form',
                'view_mode': 'form,tree',
                'res_model': 'spa.folio',
                'type': 'ir.actions.act_window',
                'target': 'current',
                'res_id': folio.id,
                'domain': [('id', '=', folio.id)],
            }


class SpaEmployee(models.Model):
    _inherit = 'spa.employee'
    _description = 'Spa Employee'

    employee_reservation_line_ids = fields.One2many('spa.employee.reservation.line',
                                                    'employee_id',
                                                    string='Employee Reserv Line')

    employee_shift_timing_ids = fields.One2many('spa.employee.shift.timing',
                                                'employee_id',
                                                string='Employee Shift Timing')

    @api.model
    def _cron_employee_line(self):
        """
        This method is for scheduler
        every 1min scheduler will call this method and check Status of
        employee is occupied or available
        --------------------------------------------------------------
        @param self: The object pointer
        @return: update status of spa employee reservation line
        """
        reservation_line_obj = self.env['spa.employee.reservation.line']
        service_line_obj = self.env['spa.service.line']
        now = datetime.now()
        curr_date = now.strftime(dt)
        for emp in self.search([]):
            if emp.employee_reservation_line_ids:
                reserv_line_ids = [reservation_line.id for
                                   reservation_line in
                                   emp.employee_reservation_line_ids]
                if reserv_line_ids:
                    reserv_args = [('id', 'in', reserv_line_ids),
                                   ('start_time', '<=', curr_date),
                                   ('end_time', '>=', curr_date)]
                    reservation_line_ids = reservation_line_obj.search(reserv_args)
                    if reservation_line_ids:
                        emp_ids = [emp_line.ids for emp_line in emp.spa_service_line_ids]
                        emp_args = [('id', 'in', emp_ids),
                                    ('ser_start_time', '<=', curr_date),
                                    ('ser_end_time', '>=', curr_date)]
                        service_line_ids = service_line_obj.search(emp_args)
                        status = {'isemployee': True, 'color': 5}
                        if reservation_line_ids.ids:
                            status = {'isemployee': False, 'color': 2}
                        emp.write(status)
                        if service_line_ids.ids:
                            status = {'isemployee': False, 'color': 2}
                        emp.write(status)
                        if reservation_line_ids.ids and service_line_ids.ids:
                            raise ValidationError(_('Wrong Entry'),
                                                  _('Please Check Employee Status \
                                             for %s.' % (emp.name)))
        return True


class SpaEmployeeReservationLine(models.Model):
    _name = 'spa.employee.reservation.line'
    _description = 'Spa Employee Reservation'
    _rec_name = 'employee_id'

    employee_id = fields.Many2one(comodel_name='spa.employee', string='Employee id')
    start_time = fields.Datetime('Check In Date', required=True)
    end_time = fields.Datetime('Check Out Date', required=True)
    state = fields.Selection([('assigned', 'Assigned'),
                              ('unassigned', 'Unassigned')], 'Employee Status')
    reserve_id = fields.Many2one('spa.reservation',
                                 string='Reservation')
    status = fields.Selection(string='state', related='reserve_id.state')
    product_id = fields.Many2one('product.product',
                                 string='Service')


class EmployeeReservationSummary(models.Model):
    _name = 'employee.reservation.summary'
    _description = 'Employee reservation summary'

    name = fields.Char('Reservation Summary', default='Reservations Summary',
                       invisible=True)
    start_time = fields.Datetime('Start Time')
    end_time = fields.Datetime('Stop Time')
    summary_header = fields.Text('Summary Header')
    employee_summary = fields.Text('Employee Summary')

    @api.model
    def default_get(self, fields):
        """
        To get default values for the object.
        @param self: The object pointer.
        @param fields: List of fields for which we want default values
        @return: A dictionary which of fields with values.
        """
        if self._context is None:
            self._context = {}
        res = super(EmployeeReservationSummary, self).default_get(fields)
        # Added default datetime as today and date to as today + 30.
        from_dt = datetime.today()
        dt_from = from_dt.strftime(dt)
        to_dt = from_dt + relativedelta(days=30)
        dt_to = to_dt.strftime(dt)
        res.update({'date_from': dt_from, 'date_to': dt_to})

        if not self.start_time and self.end_time:
            date_today = datetime.today()
            first_day = datetime(date_today.year,
                                 date_today.month, 1, 0, 0, 0)
            first_temp_day = first_day + relativedelta(months=1)
            last_temp_day = first_temp_day - relativedelta(days=1)
            last_day = datetime(last_temp_day.year,
                                last_temp_day.month,
                                last_temp_day.day, 23, 59, 59)
            date_froms = first_day.strftime(dt)
            date_ends = last_day.strftime(dt)
            res.update({'start_time': date_froms, 'end_time': date_ends})
        return res

    @api.multi
    def employee_reservation(self):
        '''
        @param self: object pointer
        '''
        mod_obj = self.env['ir.model.data']
        if self._context is None:
            self._context = {}
        model_data_ids = mod_obj.search([('model', '=', 'ir.ui.view'),
                                         ('name', '=',
                                          'view_employee_reservation_form')])
        resource_id = model_data_ids.read(fields=['res_id'])[0]['res_id']
        return {'name': _('Reconcile Write-Off'),
                'context': self._context,
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'spa.reservation',
                'views': [(resource_id, 'form')],
                'type': 'ir.actions.act_window',
                'target': 'new',
                }


# @api.onchange('start_time', 'end_time')
#     def get_employee_summary(self):
#         '''
#         @param self: object pointer
#          '''
#         res = {}
#         all_detail = []
#         employee_obj = self.env['spa.employee']
#         reservation_line_obj = self.env['spa.employee.reservation.line']
#         folio_room_line_obj = self.env['folio.room.line']
#         user_obj = self.env['res.users']
#         date_range_list = []
#         main_header = []
#         summary_header_list = ['Rooms']
#         if self.date_from and self.date_to:
#             if self.date_from > self.date_to:
#                 raise UserError(_('Please Check Time period Date From can\'t \
#                                    be greater than Date To !'))
#             if self._context.get('tz', False):
#                 timezone = pytz.timezone(self._context.get('tz', False))
#             else:
#                 timezone = pytz.timezone('UTC')
#             d_frm_obj = datetime.strptime(self.date_from, dt)\
#                 .replace(tzinfo=pytz.timezone('UTC')).astimezone(timezone)
#             d_to_obj = datetime.strptime(self.date_to, dt)\
#                 .replace(tzinfo=pytz.timezone('UTC')).astimezone(timezone)
#             temp_date = d_frm_obj
#             while(temp_date <= d_to_obj):
#                 val = ''
#                 val = (str(temp_date.strftime("%a")) + ' ' +
#                        str(temp_date.strftime("%b")) + ' ' +
#                        str(temp_date.strftime("%d")))
#                 summary_header_list.append(val)
#                 date_range_list.append(temp_date.strftime
#                                        (dt))
#                 temp_date = temp_date + timedelta(days=1)
#             all_detail.append(summary_header_list)
#             room_ids = room_obj.search([])
#             all_room_detail = []
#             for room in room_ids:
#                 room_detail = {}
#                 room_list_stats = []
#                 room_detail.update({'name': room.name or ''})
#                 if not room.room_reservation_line_ids and \
#                    not room.room_line_ids:
#                     for chk_date in date_range_list:
#                         room_list_stats.append({'state': 'Free',
#                                                 'date': chk_date,
#                                                 'room_id': room.id})
#                 else:
#                     for chk_date in date_range_list:
#                         ch_dt = chk_date[:10] + ' 23:59:59'
#                         ttime = datetime.strptime(ch_dt, dt)
#                         c = ttime.replace(tzinfo=timezone).\
#                             astimezone(pytz.timezone('UTC'))
#                         chk_date = c.strftime(dt)
#                         reserline_ids = room.room_reservation_line_ids.ids
#                         reservline_ids = (reservation_line_obj.search
#                                           ([('id', 'in', reserline_ids),
#                                             ('check_in', '<=', chk_date),
#                                             ('check_out', '>=', chk_date),
#                                             ('state', '=', 'assigned')
#                                             ]))
#                         if not reservline_ids:
#                             sdt = dt
#                             chk_date = datetime.strptime(chk_date, sdt)
#                             chk_date = datetime.\
#                                 strftime(chk_date - timedelta(days=1), sdt)
#                             reservline_ids = (reservation_line_obj.search
#                                               ([('id', 'in', reserline_ids),
#                                                 ('check_in', '<=', chk_date),
#                                                 ('check_out', '>=', chk_date),
#                                                 ('state', '=', 'assigned')]))
#                             for res_room in reservline_ids:
#                                 rrci = res_room.check_in
#                                 rrco = res_room.check_out
#                                 cid = datetime.strptime(rrci, dt)
#                                 cod = datetime.strptime(rrco, dt)
#                                 dur = cod - cid
#                                 if room_list_stats:
#                                     count = 0
#                                     for rlist in room_list_stats:
#                                         cidst = datetime.strftime(cid, dt)
#                                         codst = datetime.strftime(cod, dt)
#                                         rm_id = res_room.room_id.id
#                                         ci = rlist.get('date') >= cidst
#                                         co = rlist.get('date') <= codst
#                                         rm = rlist.get('room_id') == rm_id
#                                         st = rlist.get('state') == 'Reserved'
#                                         if ci and co and rm and st:
#                                             count += 1
#                                     if count - dur.days == 0:
#                                         c_id1 = user_obj.browse(self._uid)
#                                         c_id = c_id1.company_id
#                                         con_add = 0
#                                         amin = 0.0
#                                         if c_id:
#                                             con_add = c_id.additional_hours
# #                                        When configured_addition_hours is
# #                                        greater than zero then we calculate
# #                                        additional minutes
#                                         if con_add > 0:
#                                             amin = abs(con_add * 60)
#                                         hr_dur = abs((dur.seconds / 60))
# #                                        When additional minutes is greater
# #                                        than zero then check duration with
# #                                        extra minutes and give the room
# #                                        reservation status is reserved or
# #                                        free
#                                         if amin > 0:
#                                             if hr_dur >= amin:
#                                                 reservline_ids = True
#                                             else:
#                                                 reservline_ids = False
#                                         else:
#                                             if hr_dur > 0:
#                                                 reservline_ids = True
#                                             else:
#                                                 reservline_ids = False
#                                     else:
#                                         reservline_ids = False
#                         fol_room_line_ids = room.room_line_ids.ids
#                         chk_state = ['draft', 'cancel']
#                         folio_resrv_ids = (folio_room_line_obj.search
#                                            ([('id', 'in', fol_room_line_ids),
#                                              ('check_in', '<=', chk_date),
#                                              ('check_out', '>=', chk_date),
#                                              ('status', 'not in', chk_state)
#                                              ]))
#                         if reservline_ids or folio_resrv_ids:
#                             room_list_stats.append({'state': 'Reserved',
#                                                     'date': chk_date,
#                                                     'room_id': room.id,
#                                                     'is_draft': 'No',
#                                                     'data_model': '',
#                                                     'data_id': 0})
#                         else:
#                             room_list_stats.append({'state': 'Free',
#                                                     'date': chk_date,
#                                                     'room_id': room.id})
#
#                 room_detail.update({'value': room_list_stats})
#                 all_room_detail.append(room_detail)
#             main_header.append({'header': summary_header_list})
#             self.summary_header = str(main_header)
#             self.room_summary = str(all_room_detail)
#         return res


class QuickServiceReservation(models.TransientModel):
    _name = 'quick.service.reservation'
    _description = 'Quick Service Reservation'

    partner_id = fields.Many2one('res.partner', string="Customer", domain="[('customer','=',True)]",
                                 required=True)
    product_id = fields.Many2one('spa.services',
                                 string='Service', required=True, domain="[('type','=','service')]")
    price_unit = fields.Float('Unit Price', required=True, digits_compute=dp.get_precision('Product Price'))
    hours = fields.Integer(string="Hrs", required=True)
    mins = fields.Integer(string="Mins", required=True)
    start_time = fields.Datetime('Check In', required=True, default=fields.Datetime.now())
    end_time = fields.Datetime('Check Out', required=True)
    employee_id = fields.Many2one('spa.employee', 'Employee', required=True)
    warehouse_id = fields.Many2one('stock.warehouse', 'Branch', required=True, default=1)
    pricelist_id = fields.Many2one('product.pricelist', 'pricelist',
                                   required=True)
    partner_invoice_id = fields.Many2one('res.partner', 'Invoice Address',
                                         required=True)
    partner_order_id = fields.Many2one('res.partner', 'Ordering Contact',
                                       required=True)
    partner_shipping_id = fields.Many2one('res.partner', 'Delivery Address',
                                          required=True)

    @api.onchange('start_time', 'end_time')
    def on_change_times(self):
        if self.product_id:
            # set end time from services
            change_et = datetime.strptime(str(self.start_time), "%Y-%m-%d %H:%M:%S")
            change_et = change_et + timedelta(hours=self.product_id.hours, minutes=self.product_id.mins)
            self.end_time = datetime.strftime(change_et, "%Y-%m-%d %H:%M:%S")
            if self.end_time < self.start_time:
                raise ValidationError(
                    _('Endtime should be greater.'))

            spa_employee_obj = self.env['spa.employee']
            spa_employee_ids = spa_employee_obj.search([('allowed_services', '=', self.product_id.id)])
            emp_ids = []
            # if not self.reservation_id.partner_id:
            #     raise ValidationError(
            #                      _('Before choosing a service,\n You have to choose \
            #                          a partner in\
            #                          the reservation form.'))
            # get timing difference
            if self.start_time and self.end_time:
                start_time = datetime.strptime(str(self.start_time), "%Y-%m-%d %H:%M:%S")
                end_time = datetime.strptime(str(self.end_time), "%Y-%m-%d %H:%M:%S")
                diff = end_time - start_time
                days, seconds = diff.days, diff.seconds
                self.hours = days * 24 + seconds // 3600
                self.mins = (seconds % 3600) // 60
                for emp in spa_employee_ids:
                    assigned = False
                    buffer_time = self.float_time_convert(self.env.user.company_id.spa_buffer_time)
                    for line in emp.employee_reservation_line_ids:
                        if line.status != 'cancel':
                            # buffer_time diff

                            line_et = datetime.strptime(str(line.end_time), "%Y-%m-%d %H:%M:%S")
                            line_et = line_et + timedelta(hours=buffer_time[0], minutes=buffer_time[1])
                            buf_line_et = datetime.strftime(line_et, "%Y-%m-%d %H:%M:%S")
                            # print line.start_time,' line.start_time ',line.end_time,' line.end_time ',buf_line_et,' buf_line_et ',self.start_time,' self.start_time',self.end_time,' self.end_time'
                            if (line.start_time <= self.start_time <=
                                    buf_line_et) or (line.start_time <= self.end_time <=
                                                         buf_line_et) or (self.start_time <= line.start_time <=
                                                                              self.end_time):
                                assigned = True

                    for rm_line in emp.spa_service_line_ids:
                        if rm_line.status != 'cancel':
                            # buffer_time diff
                            rm_line_et = datetime.strptime(rm_line.ser_end_time, "%Y-%m-%d %H:%M:%S")
                            rm_line_et = rm_line_et + timedelta(hours=buffer_time[0], minutes=buffer_time[1])
                            buf_rm_line_et = datetime.strftime(rm_line_et, "%Y-%m-%d %H:%M:%S")
                            if (rm_line.ser_start_time <= self.start_time <=
                                    buf_rm_line_et) or (rm_line.ser_start_time <= self.end_time <=
                                                            buf_rm_line_et) or (
                                    self.start_time <= rm_line.ser_start_time <=
                                self.end_time):
                                assigned = True

                    att_assigned = False
                    emp_att = emp.employee_shift_timing_ids.filtered(lambda att: int(att.name) == start_time.weekday())
                    # checking whether attendance time assigned or not
                    for att in emp_att:
                        att_start_hrs = att.hour_from.split(':')  # self.float_time_convert(att.hour_from)
                        intl_st = str(start_time)[:-8] + str(att_start_hrs[0]) + ':' + str(att_start_hrs[1]) + ':00'
                        intl_st = datetime.strptime(intl_st, dt)
                        start_time_tz = datetime.strptime(str(start_time), dt).replace(tzinfo=pytz.UTC).astimezone(
                            pytz.timezone(self._context.get('tz') or 'UTC'))
                        start_time_tz = datetime.strptime(str(start_time_tz)[:19], dt)

                        att_end_hrs = att.hour_to.split(':')  # self.float_time_convert(att.hour_to)
                        intl_et = str(end_time)[:-8] + str(att_end_hrs[0]) + ':' + str(att_end_hrs[1]) + ':00'
                        intl_et = datetime.strptime(intl_et, dt)
                        end_time_tz = datetime.strptime(str(end_time), dt).replace(tzinfo=pytz.UTC).astimezone(
                            pytz.timezone(self._context.get('tz') or 'UTC'))
                        end_time_tz = datetime.strptime(str(end_time_tz)[:19], dt)
                        # print intl_st,' intl_st ',intl_et,' intl_et ',start_time_tz,' start_time_tz ',end_time_tz,' end_time_tz'
                        if (intl_st <= start_time_tz <=
                                intl_et) or (intl_st <= end_time_tz <=
                                                 intl_et) or (start_time_tz <= intl_st <=
                                                                  end_time_tz):
                            att_assigned = True

                    # leave details check
                    leave_details = emp.resource_id.calendar_id.leave_ids.filtered(
                        lambda att: att.resource_id == emp.resource_id)
                    for leave in leave_details:
                        if (leave.date_from <= self.start_time <=
                                leave.date_to) or (leave.date_from <= self.end_time <=
                                                       leave.date_to) or (self.start_time <= leave.date_from <=
                                                                              self.end_time):
                            att_assigned = False

                    if not assigned and att_assigned:
                        emp_ids.append(emp.id)
            domain = {'employee_id': [('id', 'in', emp_ids)]}
            return {'domain': domain}

    @api.multi
    def float_time_convert(self, float_val):
        factor = float_val < 0 and -1 or 1
        val = abs(float_val)
        return (factor * int(math.floor(val)), int(round((val % 1) * 60)))

    @api.onchange('product_id')
    def on_change_product_id(self):
        '''
        When you change product_id it check checkin and checkout are
        filled or not if not then raise warning
        -----------------------------------------------------------
        @param self: object pointer
        '''
        if self.product_id:
            prod = self.product_id.id
            partner = self.partner_id
            price_list = self.pricelist_id.id
            service_line_obj = self.env['spa.service.line']
            vals = {}
            domain = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
            vals['product_uom'] = self.product_id.uom_id
            vals['product_uom_qty'] = 1.0
            vals['partner_id'] = partner
            vals['pricelist'] = price_list
            product = self.product_id.with_context(
                lang=False,
                partner=partner.id,
                quantity=vals.get('product_uom_qty') or self.product_uom_qty,
                pricelist=False,
                uom=self.product_id.uom_id.id
            )

            self.price_unit = product.lst_price

        hrs = int(self.product_id.hours)
        mins = int(self.product_id.mins)
        start_time = datetime.strptime(str(self.start_time), "%Y-%m-%d %H:%M:%S")
        self.end_time = start_time + timedelta(hours=hrs, minutes=mins)

        spa_employee_obj = self.env['spa.employee']
        spa_employee_ids = spa_employee_obj.search([('allowed_services', '=', self.product_id.id)])
        emp_ids = []

        for emp in spa_employee_ids:
            assigned = False
            for line in emp.employee_reservation_line_ids:
                if line.status != 'cancel':
                    if (line.start_time <= self.start_time <=
                            line.end_time) or (line.start_time <=
                                                   self.end_time <=
                                                   line.end_time):
                        assigned = True
            for rm_line in emp.spa_service_line_ids:
                if rm_line.status != 'cancel':
                    if (rm_line.ser_start_time <= self.start_time <=
                            rm_line.ser_end_time) or (rm_line.ser_start_time <=
                                                          self.end_time <=
                                                          rm_line.ser_end_time):
                        assigned = True
            if not assigned:
                emp_ids.append(emp.id)
        domain = {'employee': [('id', 'in', emp_ids)]}
        return {'domain': domain}

    @api.onchange('partner_id')
    def onchange_partner_id_res(self):
        '''
        When you change partner_id it will update the partner_invoice_id,
        partner_shipping_id and pricelist_id of the spa reservation as well
        ---------------------------------------------------------------------
        @param self: object pointer
        '''
        if not self.partner_id:
            self.partner_invoice_id = False
            self.partner_shipping_id = False
            self.partner_order_id = False
        else:
            addr = self.partner_id.address_get(['delivery', 'invoice',
                                                'contact'])
            self.partner_invoice_id = addr['invoice']
            self.partner_order_id = addr['contact']
            self.partner_shipping_id = addr['delivery']
            self.pricelist_id = self.partner_id.property_product_pricelist.id

    @api.onchange('employee')
    def on_change_employee(self):
        if self.employee_id and self.product_id:

            assigned = False
            for line in self.employee_id.employee_reservation_line_ids:
                if line.status != 'cancel':
                    if (self.start_time <= line.start_time <=
                            self.end_time) or (self.start_time <=
                                                   line.end_time <=
                                                   self.end_time):
                        assigned = True
            for rm_line in self.employee_id.spa_service_line_ids:
                if rm_line.status != 'cancel':
                    if (self.start_time <= rm_line.ser_start_time <=
                            self.end_time) or (self.start_time <=
                                                   rm_line.ser_end_time <=
                                                   self.end_time):
                        assigned = True
            if assigned:
                raise ValidationError(_('Warning'),
                                      _(
                                          'Employee already reserved for another time slot in this period, please check n confirm'))

    @api.model
    def default_get(self, fields):
        """
        To get default values for the object.
        @param self: The object pointer.
        @param fields: List of fields for which we want default values
        @return: A dictionary which of fields with values.
        """
        if self._context is None:
            self._context = {}
        res = super(QuickServiceReservation, self).default_get(fields)
        if self._context:
            keys = self._context.keys()
            if 'date' in keys:
                res.update({'start_time': self._context['date']})
            if 'employee_id' in keys:
                empid = self._context['employee_id']
                res.update({'employee_id': int(empid)})
        return res

    @api.multi
    def service_reserve(self):
        """
        This method create a new record for spa.reservation
        -----------------------------------------------------
        @param self: The object pointer
        @return: new record set for hotel reservation.
        """
        spa_res_obj = self.env['spa.reservation']
        for res in self:
            rec = (spa_res_obj.create
                   ({'partner_id': res.partner_id.id,
                     'partner_invoice_id': res.partner_invoice_id.id,
                     'partner_order_id': res.partner_order_id.id,
                     'partner_shipping_id': res.partner_shipping_id.id,
                     'start_time': res.start_time,
                     'end_time': res.end_time,
                     'warehouse_id': res.warehouse_id.id,
                     'pricelist_id': res.pricelist_id.id,
                     'reservation_line': [(0, 0,
                                           {'product_id': res.product_id.id,
                                            'categ_id': res.product_id.categ_id.id,
                                            'price_unit': res.price_unit,
                                            'hours': res.hours,
                                            'mins': res.mins,
                                            'start_time': res.start_time,
                                            'end_time': res.end_time,
                                            'employee': res.employee_id.id,
                                            'name': (res.product_id and
                                                     res.product_id.name or '')
                                            })]
                     }))
        return rec


class SpaEmployeeShiftTiming(models.Model):
    _name = "spa.employee.shift.timing"
    _description = "Shift Timing"
    _order = 'name, hour_from'

    name = fields.Selection([
        ('0', 'Monday'),
        ('1', 'Tuesday'),
        ('2', 'Wednesday'),
        ('3', 'Thursday'),
        ('4', 'Friday'),
        ('5', 'Saturday'),
        ('6', 'Sunday')
    ], 'Day of Week', required=True, index=True, default='0')
    hour_from = fields.Selection(SHIFT_TIMING, string='Shift from', required=True, index=True,
                                 help="Start and End time of working.")
    hour_to = fields.Selection(SHIFT_TIMING, string='Shift to', required=True)
    employee_id = fields.Many2one("spa.employee", string="Employee")
