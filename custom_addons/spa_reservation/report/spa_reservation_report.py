# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil import parser
from odoo import models, fields, api


class ReservationDetailReport(models.AbstractModel):
    _name = "report.spa_reservation.report_serviceres_qweb"

    def get_data(self, date_start, date_end):
        reservation_obj = self.pool.get('spa.reservation')
        tids = reservation_obj.search(self.cr, self.uid,
                                      [('start_time', '>=', date_start),
                                       ('end_time', '<=', date_end)])
        res = reservation_obj.browse(self.cr, self.uid, tids)
        return res

    @api.multi
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        act_ids = self.env.context.get('active_ids', [])
        docs = self.env[self.model].browse(act_ids)
        date_start = data.get('start_time', fields.Date.today())
        date_end = data['form'].get('end_time', str(datetime.now() +
                                    relativedelta(months=+1,
                                                  day=1, days=-1))[:10])
        rm_act = self.with_context(data['form'].get('used_context', {}))
        get_data = rm_act._get_data(date_start, date_end)

        docargs = {
            'doc_ids': docids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'time': time,
            'get_data': get_data,
        }
        docargs['data'].update({'date_end':
                                parser.parse(docargs.get('data').
                                             get('date_end')).
                                strftime('%m/%d/%Y')})
        docargs['data'].update({'date_start':
                                parser.parse(docargs.get('data').
                                             get('date_start')).
                                strftime('%m/%d/%Y')})
        render_model = 'spa_reservation.report_serviceres_qweb'
        return self.env['report'].render(render_model, docargs)



