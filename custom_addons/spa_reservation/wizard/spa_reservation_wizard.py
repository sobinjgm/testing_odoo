# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api


class SpaReservationWizard(models.TransientModel):
    _name = 'spa.reservation.wizard'

    date_start = fields.Datetime('Start Date', required=True)
    date_end = fields.Datetime('End Date', required=True)

    @api.multi
    def report_reservation_detail(self):
        data = {
            'ids': self.ids,
            'model': 'spa.reservation',
            'form': self.read(['date_start', 'date_end'])[0]
        }
        return self.env['report'
                        ].get_action(self,
                                     'spa_reservation.report_serviceres_qweb',
                                     data=data)




class MakeFolioWizard(models.TransientModel):

    _name = 'wizard.make.folio'

    grouped = fields.Boolean('Group the Folios')

    @api.multi
    def makeFolios(self):
        order_obj = self.env['spa.reservation']
        newinv = []
        for order in order_obj.browse(self._context['active_ids']):
            for folio in order.folio_id:
                newinv.append(folio.id)
        return {
            'domain': "[('id','in', [" + ','.join(map(str, newinv)) + "])]",
            'name': 'Folios',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'spa.folio',
            'view_id': False,
            'type': 'ir.actions.act_window'
        }
