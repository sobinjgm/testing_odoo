# See LICENSE file for full copyright and licensing details.

{
    'name': 'spa report',
    'version': '12.0.1.0.0',
    'author': 'Sobin Joseph GM ',
    'category': 'spa report',
    'website': '',
    'depends': ['base', 'sale'],
    'license': 'AGPL-3',
    'summary': 'report for sale order',
    'data': [
            'report/spa_report.xml',
            'report/report_spa.xml',
            'views/spa_view.xml',
            'wizard/spa_wizard.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True
}
