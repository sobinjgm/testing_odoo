# See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from dateutil.relativedelta import relativedelta
from dateutil import parser
from odoo import api, fields, models


class FolioReport(models.AbstractModel):
    _name = 'report.spa_reserve.report_spa_order_order'

    def get_data(self, date_start, date_end, source):
        total_amount = 0.0
        data_folio = []
        spa_obj = self.env['spa.reservation']
        print(source, '\n\n\n')
        if('all'==source):
            act_domain = [('create_date', '>=', date_start),
                          ('create_date', '<=', date_end)]
        else:
            act_domain = [('create_date', '>=', date_start),
                      ('create_date', '<=', date_end),('source', '=', source)]
                         
        tids = spa_obj.search(act_domain)
        for data in tids:
            print(data)
            data_folio.append({
                'name': data.partner_id.name,
                'service': data.reservation_line.name, 
                'product_name': data.reservation_line.product_id.name,
                'price':data.reservation_line.price_unit,
            })
            total_amount += data.reservation_line.price_unit
        data_folio.append({'total_amount': total_amount})
        return data_folio

    @api.model
    def _get_report_values(self, docids, data):
        self.model = self.env.context.get('active_model')
        
        if data is None:
            data = {}
        if not docids:
            docids = data['form'].get('docids')
        folio_profile = self.env['spa.reservation'].browse(docids)
        date_start = data['form'].get('date_start', fields.Date.today())
        date_end = data['form'].get(
            'date_end', str(datetime.now() + relativedelta(
                months=+1, day=1, days=-1))[:10])
        source =  data['form'].get('source')     
        return {
            'doc_ids': docids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': folio_profile,
            'time': time,
            'spa_report_data': self.get_data(date_start, date_end,source)
        }
