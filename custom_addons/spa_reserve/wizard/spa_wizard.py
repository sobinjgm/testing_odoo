# See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from odoo.exceptions import UserError


MODES = [
   ('direct', 'Direct'),('mobile', 'Mobile'), ('web', 'Website'),('all','All')
]
        
class FolioReportWizard(models.TransientModel):
    _name = 'spa.report.wizard'
    _rec_name = 'date_start'

    date_start = fields.Datetime('Start Date')
    date_end = fields.Datetime('End Date')
#    employee_ids = fields.Many2many('spa.employee',string='Employee')
    source = fields.Selection(MODES, default='All')

    @api.multi
    def print_report(self):
        print('hi')
        data = {
            'ids': self.ids,
            'model': 'spa.reservation',
            'form': self.read(['date_start', 'date_end','source'])[0]
        }
        spa_obj = self.env['spa.reservation']
        if(self.source == 'all'):
            act_domain = [('create_date', '>=', self.date_start),
                          ('create_date', '<=', self.date_end)]
        else:
            act_domain = [('create_date', '>=', self.date_start),
                      ('create_date', '<=', self.date_end),('source', '=', self.source)]
                         
        tids = spa_obj.search(act_domain)
        if len(tids) == 0:
            raise UserError("No records found")
        print('hi 2')    
        return self.env.ref('spa_reserve.report_spa_custom').\
            report_action(self, data=data)
