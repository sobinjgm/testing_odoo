# -*- coding: utf-8 -*-
# Copyright (C) 2017-Today  Technaureus Info Solutions(<http://technaureus.com/>).
{
    'name': 'POS Receipt Arabic',
    'version': '1.0',
    'sequence': 1,
    'category': 'Point of Sale',
    'summary': 'POS Receipt Arabic',
    'author': 'Technaureus Info Solutions',
    'website': 'http://www.technaureus.com/',
    'price': 75,
    'currency': 'EUR',
    'license': 'Other proprietary',
    'description': """
This module is for POS receipt
        """,
    'depends': ['sa_uae_vat','point_of_sale'],
    'qweb': ['static/src/xml/templates.xml'],
    'data': ['views/pos_receipt_template.xml'],
    'images': ['images/main_screenshot.png'],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
